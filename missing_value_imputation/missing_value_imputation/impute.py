import argparse
from .imputers.cross_validation import save_imputer
from .imputers.base import load_and_impute
import logging


parser = argparse.ArgumentParser()
parser.add_argument("-f", "--fit_data", help="data to fit the imputer")
parser.add_argument("-d", "--impute_data", help="data to be imputed")
parser.add_argument("-i", "--imputer", help="path for a fitted imputer")
parser.add_argument("-o", "--output", help="output path to save imputer or imputed data")
parser.add_argument("-r", "--random_state", help="Seed for the random folds.", default=1234, type=int)
args = parser.parse_args()


def fit():
    logging.basicConfig(filename=args.output.split('.json')[0] + '-fit.log', filemode='w', level=logging.WARNING)
    save_imputer(data_path=args.fit_data, out_path=args.output, seed=args.random_state)
    print('SUCCESS !')


def impute():
    logging.basicConfig(filename=args.output.split('.csv')[0] + '-impute.log', filemode='w', level=logging.WARNING)
    load_and_impute(args)
    print('SUCCESS !')

