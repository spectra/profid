import pandas as pd
import json
import logging
import numpy as np
from .fkm_imputer import FuzzyKMeansEstimator


def import_data(filename=None):
    data = pd.read_csv(filename)
    data = pd.concat([data['ID'], data.drop(columns=['ID', 'Survival_time', 'Status']),
                      data[['Survival_time', 'Status']]], axis=1)
    return data


def load_and_impute(args):
    # load data
    to_impute = import_data(args.impute_data)
    to_impute_noid = to_impute.drop(columns=['ID'])
    with open(args.imputer, 'r') as fp:
        fkm_data = json.load(fp)
    fkm = FuzzyKMeansEstimator(c=fkm_data['params']['c'], m=fkm_data['params']['m'])
    fkm.centers = np.array(fkm_data['centers'])
    fkm.out_columns = pd.Index(fkm_data['out_columns'])
    if to_impute_noid.columns.to_list() != fkm_data['out_columns']:
        err = 'Variable names are not matching with imputer'
        logging.critical(err)

    # impute missing data
    df_imputed = fkm.transform(to_impute_noid.drop(columns=['Survival_time', 'Status']))
    df_imputed.columns = to_impute_noid.columns[:-2]
    df_imputed = pd.concat([to_impute['ID'], df_imputed, to_impute[['Survival_time', 'Status']]], axis=1)
    df_imputed.to_csv(args.output, index=False)

