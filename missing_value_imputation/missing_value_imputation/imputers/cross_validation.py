from sklearn.model_selection import StratifiedKFold
from sklearn.pipeline import Pipeline
import pandas as pd
import numpy as np
from hyperopt import fmin, tpe, hp, Trials
from lifelines.utils.sklearn_adapter import sklearn_adapter
from lifelines import CoxPHFitter
from functools import partial
from sksurv.util import Surv
from sksurv.metrics import integrated_brier_score
from .fkm_imputer import FuzzyKMeansEstimator
from .transforms import DropColumns
from .base import import_data
import logging
import json


def brier_isw(pipe, data):
    t_cens_test_max = data[data.Status == 0].Survival_time.max()
    data_trunc = data[data.Survival_time <= t_cens_test_max].reset_index()
    Y_surv = data_trunc[data_trunc.Status == 1].Survival_time
    
    t_min = Y_surv.min()
    t_max = np.floor(Y_surv.max())
    
    survival_times = np.arange(t_min, t_max)
    pred = np.transpose([pipe.named_steps['cph'].lifelines_model.predict_survival_function(data_trunc.drop(['Survival_time', 'Status'], axis=1), survival_times).values])[:, :, 0]
    y_test = Surv.from_arrays(data_trunc.Status == 1 , data_trunc.Survival_time)
    return integrated_brier_score(y_test, y_test, pred, survival_times)


def get_score(X, Y, seed, hyperparams):
    params = {'c': int(hyperparams['c']), 
              'm': hyperparams['m']}
    cv = StratifiedKFold(n_splits=5, shuffle=True, random_state=seed)
    
    scores = []
    
    CoxRegression = sklearn_adapter(CoxPHFitter, event_col='Status')
    for train_index, test_index in cv.split(X, X.Status):

        X_train = X.iloc[train_index]
        X_test = X.iloc[test_index]
        Y_train = Y.iloc[train_index]
        
        pipe = Pipeline(steps=[('fkm', FuzzyKMeansEstimator(**params)), ('d', DropColumns(cols=['Survival_time'])),
                               ('cph', CoxRegression(penalizer=0.1))])
        pipe.fit(X_train, Y_train)
        data = pd.concat([pipe.named_steps['fkm'].transform(X_test)], axis = 1)
        
        ibs = brier_isw(pipe, data)
        scores.append(ibs)
    
    score = np.mean(scores)

    return score


def get_best_params(fit_data, seed=None):
    space={'c': hp.randint('c', 2, 10),
           'm': hp.uniform('m', 1.1, 3)
    }

    trials = Trials()
    X = fit_data.copy()
    Y = fit_data['Survival_time']
    X.loc[X.Status != 1, 'Status'] = 0
    objective = partial(get_score, X, Y, seed)

    best=fmin(fn=objective, 
            space=space, 
            algo=tpe.suggest, 
            max_evals=25, 
            trials=trials, 
            rstate=np.random.RandomState(seed) 
        )
    
    return best


def save_imputer(data_path, out_path, seed=None):
    fit_data = import_data(data_path)
    fit_data.drop(columns=['ID'], inplace=True)
    logging.warning('Input data dimension (without ID) is: {}'.format(fit_data.shape))
    logging.warning('Input columns for imputation (without ID) are: {}'.format(fit_data.columns.to_list()))
    logging.warning('Number of missing values per input column are: {}'.format({c:fit_data[c].isna().sum() for c in fit_data.columns}))
    best = get_best_params(fit_data, seed=seed)
    logging.warning('Optimal number of clusters is {} and optimal fuzziness is {}'.format(best['c'], round(best['m'], 2)))
    fkm_imputer = FuzzyKMeansEstimator(**best).fit(fit_data)
    fkm_data = {'params': {'c': int(fkm_imputer.c), 'm': float(fkm_imputer.m)}, 'centers': fkm_imputer.centers.tolist(),
     'out_columns': fkm_imputer.out_columns.to_list()}
    with open(out_path, 'w') as fp:
        json.dump(fkm_data, fp)

