from sklearn.base import TransformerMixin, BaseEstimator
import numpy as np


class PandasTransformMixin(object):
    """Mixin to allow scikit learn transformers to input and output pandas dataframes"""

    out_prefix = 'feature'
    out_columns = None

    def fit(self, X, y=None, **kwargs):
        try:
            y_ = y.values
        except AttributeError:
            y_ = y

        try:
            self.out_columns = X.columns
            X_ = X.values
        except AttributeError:
            self.out_columns = None
            X_ = X

        return super(PandasTransformMixin, self).fit(X_, y_, **kwargs)

    def transform(self, X, y=None):

        try:
            index = X.index
            cols = X.columns
            X_ = X.values
        except AttributeError:
            index = None
            cols = None
            X_ = X.copy()

        if y is not None:
            out_vals = super(PandasTransformMixin, self).transform(X_, y=y)
        else:
            out_vals = super(PandasTransformMixin, self).transform(X_)

        if index is None or cols is None:
            return out_vals

        import pandas as pd

        if self.out_columns is not None and len(self.out_columns) == out_vals.shape[1]:
            out_cols = self.out_columns
        else:
            if out_vals.shape[1] == 1:
                out_cols = [self.out_prefix]
            else:
                out_cols = ['{}_{}'.format(self.out_prefix, i) for i in range(out_vals.shape[1])]

        return pd.DataFrame(out_vals, index=index, columns=out_cols)

    def fit_transform(self, X, y=None, **kwargs):
        self.fit(X, y, **kwargs)
        return self.transform(X, y=y)

class ReCategorizer(TransformerMixin, BaseEstimator):

    def __init__(self, common_cols_base_types):
        self.common_cols_base_types = common_cols_base_types

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        X_ = X.copy()
        for c in self.common_cols_base_types.keys():
            if self.common_cols_base_types[c]:
                matching_cols = [col for col in X_.cols if c in col]
                max_c = X_[matching_cols].max(axis=1)
                for col in matching_cols:
                    X_.loc[:, col] = (X_.loc[:, col] == max_c).astype(int)
        return X_

class DropColumns(BaseEstimator, TransformerMixin):

    def __init__(self, cols):
        if not isinstance(cols, list):
            self.cols = [cols]
        else:
            self.cols = cols

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        X = X.copy()
        return X.drop(columns=self.cols)
