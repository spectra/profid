import numpy as np
from sklearn.base import TransformerMixin
from .transforms import PandasTransformMixin
from .bpca import BPCA


class BPCAEstimatorNP(TransformerMixin):

    def __init__(self, probabilistic=False, probabilistic_inv=False, n_iteration=100, max_err=None,
                 verbose=False, print_every=50):
        self._pca = BPCA()
        self._probabilistic = probabilistic
        self.probabilistic_inv = probabilistic_inv
        self._n_iteration = n_iteration
        self._max_err = max_err
        self._verbose = verbose,
        self._print_every = print_every

    def impute_means(self, X):

        X_ = X.copy()
        missing_mask = np.isnan(X_)
        col_means_array = np.tile(self.col_means, (X_.shape[0], 1))
        X_[missing_mask] = col_means_array[missing_mask]
        X_ = np.nan_to_num(X_)
        return X_, missing_mask

    def fit(self, X, y=None, complete_data=None):

        try:
            cdata = complete_data.values
        except AttributeError:
            cdata = complete_data

        mse = np.zeros(self._n_iteration)

        X_ = X.copy()
        self.col_means = np.nanmean(X_, axis=0)
        X_, missing_mask = self.impute_means(X)

        for i in range(self._n_iteration):
            self._pca.fit(X_)
            X_[missing_mask] = self._pca.inverse_transform(self._pca.transform(X_))[missing_mask]
            if cdata is not None:
                mse[i] = np.sum((cdata - X_) ** 2) / cdata.shape[0]
                if self._verbose and i % self._print_every == 0:
                    print('Iter %d, MSE=%f' % (i, mse[i]))

                if self._max_err is not None:
                    if np.abs(mse[i - 1] - mse[i]) < self._max_err:
                        break

        return self

    def transform(self, X, y=None):
        X_, missing_mask = self.impute_means(X)
        X_[missing_mask] = self._pca.inverse_transform(self._pca.transform(X_, probabilistic=self.probabilistic_inv),
                                                       probabilistic=self._probabilistic)[missing_mask]
        return X_

    def transform_imputed(self, X, X_imp):
        X_ = np.nan_to_num(X_imp)
        missing_mask = np.isnan(X)
        X_[missing_mask] = self._pca.inverse_transform(self._pca.transform(X_, probabilistic=self.probabilistic_inv),
                                                       probabilistic=self._probabilistic)[missing_mask]
        return X_


class BayesianPCAEstimator(PandasTransformMixin, BPCAEstimatorNP):
    pass
