import pytest
import pandas as pd
import numpy as np
from missing_value_imputation.imputers import FuzzyKMeansEstimator
from missing_value_imputation.imputers import BayesianPCAEstimator
from sklearn.metrics import mean_squared_error


@pytest.fixture
def fkm():
    fkm = FuzzyKMeansEstimator(c=3, m=2, maxiter=10, error=0.001)
    return fkm


@pytest.fixture
def bpca():
    bpca = BayesianPCAEstimator(n_iteration=10, max_err=0.001)
    return bpca


@pytest.fixture
def full_data():
    data = pd.DataFrame(data=[[1, 1, 1], [2, 2, 2], [3, 3, 3]])
    return data


@pytest.fixture
def amp_data():
    data = pd.DataFrame(data=[[1, 1, 1], [2, np.nan, 2], [np.nan, 3, 3]])
    return data


@pytest.fixture
def amp_data_patterns():
    data = pd.DataFrame(data=[[1, 1.5, 1], [1, np.nan, 1], [2, 2, 2], [3, 3, np.nan], [np.nan, 0, np.nan],
                              [np.nan, np.nan, np.nan]])
    return data


@pytest.fixture
def amp_data_patterns_numpy():
    data = np.array([[1, 1.5, 1], [1, np.nan, 1], [2, 2, 2], [3, 3, np.nan], [np.nan, 0, np.nan],
                    [np.nan, np.nan, np.nan]])
    return data


def test_fkm_impute_without_missing(fkm, full_data):
    data_imputed = fkm.fit_transform(full_data)
    assert mean_squared_error(data_imputed, full_data) == 0


def test_bpca_impute_without_missing(bpca, full_data):
    data_imputed = bpca.fit_transform(full_data)
    assert mean_squared_error(data_imputed, full_data) == 0


def test_fkm_impute_with_missing(fkm, full_data, amp_data):
    data_imputed = fkm.fit(full_data).transform(amp_data)
    assert np.all(data_imputed > 0)


def test_bpca_impute_with_missing(bpca, full_data, amp_data):
    data_imputed = bpca.fit(full_data).transform(amp_data)
    assert np.all(data_imputed > 0)


def test_fkm_impute_with_missing_patterns(fkm, amp_data_patterns, amp_data_patterns_numpy):
    data_imputed = fkm.fit_transform(amp_data_patterns)
    missing = np.isnan(data_imputed.values)
    assert missing.sum() == 0

    data_imputed = fkm.fit_transform(amp_data_patterns_numpy)
    missing = np.isnan(data_imputed)
    assert missing.sum() == 0


def test_bpca_impute_with_missing_patterns(bpca, amp_data_patterns, amp_data_patterns_numpy):
    data_imputed = bpca.fit_transform(amp_data_patterns)
    missing = np.isnan(data_imputed.values)
    assert missing.sum() == 0

    data_imputed = bpca.fit_transform(amp_data_patterns_numpy)
    missing = np.isnan(data_imputed)
    assert missing.sum() == 0