from setuptools import setup, find_packages


setup(
    name='missing_value_imputation',
    version='0.1.1',
    packages=find_packages(),
    install_requires=[
        "pandas==0.25.3",
        "numpy==1.19.5",
        "scikit_learn==0.22.1",
        "lifelines==0.25.11",
        "hyperopt==0.2.5",
        "scikit-survival==0.13.1"],
    zip_safe=False,
    include_package_data=True,
    entry_points={
        'console_scripts': ['fkm_fit=missing_value_imputation.impute:fit',
                            'fkm_impute=missing_value_imputation.impute:impute']
    },
    python_requires='>=3.6'
)
