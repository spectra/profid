import pandas as pd
import numpy as np
from missing_value_imputation.imputers import FuzzyKMeansEstimator

if __name__ == "__main__" and __package__ is None:
    # Create rows where features are all ones or all zeros
    df = np.concatenate([2*np.ones([10, 3]), np.ones([10, 3]), np.zeros([10, 3])])
    df_missing = df.copy()

    # Randomly make some data missing and turn into pandas dataframe
    mask = np.random.choice([True, False], size=[df.shape[0], 3], p=[0.1, 0.9])
    df_missing[mask] = np.nan
    df_missing = pd.DataFrame(df_missing)
    print(' Data with missing values:\n', df_missing)

    # fit FKM
    fkm = FuzzyKMeansEstimator(c=3, m=2, error=1e-5, maxiter=100)
    fkm = fkm.fit(df_missing)
    print('The cluster centers are: \n', fkm.centers)

    # impute missing data
    df_imputed = fkm.transform(df_missing)
    print('Data with imputed values:\n', df_imputed)

