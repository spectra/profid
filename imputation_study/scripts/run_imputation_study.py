"""
Run imputation study: compare imputers based on their effect on predictive performance. In missing_value_imputation, the
imputers are instead compared based on the accuracy of imputation.
"""
import argparse
from imputation_study import get_imputation_scores
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--data", help="path for aston data for protocol 1 or french data for protocol 2", type=str)
parser.add_argument("-f", "--french_regions", help="path for the french regions data for protocol 2", type=str)
parser.add_argument("-n", "--protocol", help="protocol number: 1 or 2",  type=int)
parser.add_argument("-p", "--prop", help="proportion of missingness for the amputer", default='0.1', type=str)
parser.add_argument("-m", "--mech", help="mechanism of missingness for the amputer", default='MAR', type=str)
parser.add_argument("-o", "--output", help="path for the results directory")
parser.add_argument("-r", "--seed", help="Seed for the random folds.", default=None, type=int)
parser.add_argument("-s", "--score", help="Scoring metric, either 'concordance_l' or 'brier_isw'.",
                    default='concordance_l', type=str)
args = parser.parse_args()

if __name__ == "__main__":
    mechanisms = args.mech.split(' ')
    proportions = [float(prop) for prop in args.prop.split(' ')]
    get_imputation_scores(protocol=args.protocol, filename=args.data, filename_regions=args.french_regions,
                          prop=proportions, mech=mechanisms,
                          seed=args.seed, score=args.score, out_path=args.output)

