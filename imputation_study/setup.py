from setuptools import setup, find_packages


setup(
    name='imputation_study',
    version='0.0.1',
    packages=find_packages(),
    install_requires=[
        "pandas==0.25.3",
        "numpy==1.18.2",
        "rpy2==3.3.2",
        "scikit_learn==0.22.1",
        "lifelines==0.25.11",
        "hyperopt==0.2.5"],
    zip_safe=False,
    include_package_data=True,
    entry_points={
        'console_scripts': ['imputation_test=imputation_study.imputation_test:main',
                            'cross_validate=imputation_study.cross_validate:main']
    }
)
