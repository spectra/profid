from sklearn.model_selection import StratifiedKFold
from lifelines import CoxPHFitter
from .imputation_utils import *


def imputation_cph_regions(df, common_cols_base_types, seed=None, score='concordance_l', out_path=None):
    regions = df.Implant_centre.unique().tolist()
    common_cols_base = common_cols_base_types.keys()

    score_mean_cph2 = np.zeros([len(common_cols_base), len(regions), 2, 5])
    score_fkm_cph2 = np.zeros([len(common_cols_base), len(regions), 2, 5])
    score_bpca_cph2 = np.zeros([len(common_cols_base), len(regions), 2, 5])

    cv = StratifiedKFold(n_splits=5, shuffle=True, random_state=seed)

    # Fit model on each systematically missing data pattern
    for idx_c, c in enumerate(common_cols_base):
        matching_cols = get_matching_cols(df, c, common_cols_base_types)
        print(matching_cols)
        for idx_r, reg in enumerate(regions):
            print('Starting iteration for feature ' + c + ' and regions up to ' + reg)
            df_c = df.copy()

            if idx_r >= len(regions) - 1:
                df_c.drop(columns=matching_cols, inplace=True)
            else:
                df_c.loc[df_c.Implant_centre.isin(regions[:idx_r + 1]), matching_cols] = np.nan

            for idx_fold, indexes in enumerate(cv.split(df_c, df_c.Status)):
                df_train_mean_imputed, df_test_mean_imputed, df_train_fkm_imputed, df_test_fkm_imputed,\
                df_train_bpca_imputed, df_test_bpca_imputed = \
                fit_imputers_cph(df_c, indexes, common_cols_base_types, c, matching_cols)
                for k in [1, 2]:

                    df_train_mean_imputed_k, df_test_mean_imputed_k = \
                        censor_competing_risk(df_train_mean_imputed, df_test_mean_imputed, k)

                    df_train_fkm_imputed_k, df_test_fkm_imputed_k = \
                        censor_competing_risk(df_train_fkm_imputed, df_test_fkm_imputed, k)

                    df_train_bpca_imputed_k, df_test_bpca_imputed_k = \
                        censor_competing_risk(df_train_bpca_imputed, df_test_bpca_imputed, k)

                    cph_mean = CoxPHFitter(penalizer=0.001).fit(df_train_mean_imputed_k, 'Survival_time', 'Status_k')
                    score_mean_cph2[idx_c, idx_r, k - 1, idx_fold] = \
                    score_cph(cph_mean, df_test_mean_imputed_k, score=score)

                    cph_fkm = CoxPHFitter(penalizer=0.001).fit(df_train_fkm_imputed_k, 'Survival_time', 'Status_k')
                    score_fkm_cph2[idx_c, idx_r, k - 1, idx_fold] = \
                    score_cph(cph_fkm, df_test_fkm_imputed_k, score=score)

                    cph_bpca = CoxPHFitter(penalizer=0.001).fit(df_train_bpca_imputed_k, 'Survival_time', 'Status_k')
                    score_bpca_cph2[idx_c, idx_r, k - 1, idx_fold] = \
                    score_cph(cph_bpca, df_test_bpca_imputed_k, score=score)

    np.save(out_path + 'score_mean_cph2_' + score, score_mean_cph2)
    np.save(out_path + 'score_fkm_cph2_' + score, score_fkm_cph2)
    np.save(out_path + 'score_bpca_cph2_' + score, score_bpca_cph2)

    return "SUCCESS"
