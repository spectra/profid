import pandas as pd

french = 'french-icd'
aston = 'aston-rdb'
israeli = 'israeli-icd'


def get_recategorization_columns(df):
    rc_columns_types = {}
    df_noid = df.drop(columns=['ID', 'Survival_time', 'Status'])
    cat_cols = df_noid.select_dtypes(include=['object', 'category']).columns.to_list()
    non_cat_cols = df_noid.select_dtypes(exclude=['object', 'category']).columns.to_list()
    for col in non_cat_cols:
        rc_columns_types[col] = {'is_categorical': False,
                                 'nb_cat':  1}
    for col in cat_cols:
        rc_columns_types[col] = {'is_categorical': True,
                                 'nb_cat':  len(df_noid[col].value_counts())}
    return rc_columns_types


def import_french_data(filename=None, regions=False, filename_regions=None):
    french_data = pd.read_csv(filename)
    french_data = french_data.loc[:, french_data.isnull().mean() <= .2].dropna()
    rc_columns_types = get_recategorization_columns(french_data)
    french_data.rename(columns={'QRS': 'QRS_cat'}, inplace=True)
    if regions:
        french_regions = pd.read_csv(filename_regions)
        french_data = french_data.merge(french_regions[['ID', 'Implant_centre']], on=['ID'])
        fr_encoded = pd.get_dummies(french_data.drop(columns=['ID', 'Implant_centre', 'Survival_time', 'Status']))
        fr_encoded = pd.concat([fr_encoded, french_data[['Survival_time', 'Status', 'Implant_centre']]],
                               axis=1)
    else:
        fr_encoded = pd.get_dummies(french_data.iloc[:, 1:])
        fr_encoded['Country_ID'] = 1
    fr_encoded = fr_encoded.reset_index(drop=True)
    return fr_encoded, rc_columns_types


def import_aston_data(filename=None):
    aston_data = pd.read_csv(filename).dropna()
    aston_data.rename(columns={'MRI_LVEF': 'Ejection_fraction'}, inplace=True)
    aston_data.loc[aston_data['QRS'] > 150, 'QRS_cat'] = '>150'
    aston_data.loc[(aston_data['QRS'] >= 120) & (aston_data['QRS'] <= 150), 'QRS_cat'] = '120-150'
    aston_data.loc[aston_data['QRS'] < 120, 'QRS_cat'] = '<120'
    aston_encoded = pd.get_dummies(aston_data.iloc[:, 1:])
    aston_encoded['Country_ID'] = 2
    return aston_encoded


def import_israeli_data(filename=None):
    israeli_data = pd.read_csv(filename).dropna()
    israeli_data.rename(columns={'LVEF': 'Ejection_fraction'}, inplace=True)
    israeli_data.loc[israeli_data['QRS_duration'] > 150, 'QRS_cat'] = '>150'
    israeli_data.loc[
        (israeli_data['QRS_duration'] >= 120) & (israeli_data['QRS_duration'] <= 150), 'QRS_cat'] = '120-150'
    israeli_data.loc[israeli_data['QRS_duration'] < 120, 'QRS_cat'] = '<120'
    israeli_encoded = pd.get_dummies(israeli_data.iloc[:, 1:])
    israeli_encoded['Country_ID'] = 3
    return israeli_encoded


def import_data(filename=None):
    data = pd.read_csv(filename)
    data = data.loc[:, data.isnull().mean() <= .2].dropna()
    rc_columns_types = get_recategorization_columns(data)
    data_encoded = pd.get_dummies(data.iloc[:, 1:])
    df = pd.concat([data_encoded.drop(columns=['Survival_time', 'Status']),
                    data_encoded[['Survival_time', 'Status']]], axis=1)
    df = df.reset_index(drop=True)
    return df, rc_columns_types


def import_stacked_data(fr_filename=None, aston_filename=None):
    """
        Import and stack french and aston datasets for imputation study
    """
    aston_df = import_aston_data(filename=aston_filename)
    french_df = import_french_data(filename=fr_filename)

    common_cols = list(set(aston_df.columns) & set(french_df.columns))

    df = pd.concat([french_df[common_cols], aston_df[common_cols]])
    df = pd.concat([df.drop(columns=['Country_ID', 'Survival_time', 'Status']),
                    df[['Country_ID', 'Survival_time', 'Status']]], axis=1)
    df = df.reset_index(drop=True)

    return df
