from sklearn.model_selection import StratifiedKFold
from lifelines import CoxPHFitter
from .imputers.base import ampute_features
from .imputation_utils import *


def imputation_cph_comparison(df, common_cols_base_types, prop=None, mech=None, seed=None, score='concordance_l',
                              out_path=None):
    common_cols_base = common_cols_base_types.keys()

    if prop is None:
        prop = [0.5]
    if mech is None:
        mech = ['MCAR']

    score_mean_cph1 = np.zeros([len(common_cols_base), len(prop), len(mech), 2, 5])
    score_fkm_cph1 = np.zeros([len(common_cols_base), len(prop), len(mech), 2, 5])
    score_bpca_cph1 = np.zeros([len(common_cols_base), len(prop), len(mech), 2, 5])
    score_full = np.zeros([2, 5])

    cv = StratifiedKFold(n_splits=5, shuffle=True, random_state=seed)

    for idx_c, c in enumerate(common_cols_base):
        print('Starting iteration for column ' + c)
        print(matching_cols)
        matching_cols = get_matching_cols(df, c, common_cols_base_types)
        for idx_p, p in enumerate(prop):
            for idx_m, m in enumerate(mech):
                print(m, p)
                df_c = df.copy().astype(float)
                df_c = ampute_features(df_c, matching_cols, p, m)
                for idx_fold, indexes in enumerate(cv.split(df_c, df_c.Status)):
                    df_train_mean_imputed, df_test_mean_imputed, df_train_fkm_imputed, df_test_fkm_imputed, \
                    df_train_bpca_imputed, df_test_bpca_imputed = \
                        fit_imputers_cph(df_c, indexes, common_cols_base_types, c, matching_cols)
                    for k in [1, 2]:
                        df_train_mean_imputed_k, df_test_mean_imputed_k = \
                            censor_competing_risk(df_train_mean_imputed, df_test_mean_imputed, k)

                        df_train_fkm_imputed_k, df_test_fkm_imputed_k = \
                            censor_competing_risk(df_train_fkm_imputed, df_test_fkm_imputed, k)

                        df_train_bpca_imputed_k, df_test_bpca_imputed_k = \
                            censor_competing_risk(df_train_bpca_imputed, df_test_bpca_imputed, k)

                        cph_mean = CoxPHFitter(penalizer=0.001).fit(df_train_mean_imputed_k, 'Survival_time', 'Status_k')
                        score_mean_cph1[idx_c, idx_p, idx_m, k - 1, idx_fold] = \
                            score_cph(cph_mean, df_test_mean_imputed_k, score=score)

                        cph_fkm = CoxPHFitter(penalizer=0.001).fit(df_train_fkm_imputed_k, 'Survival_time', 'Status_k')
                        score_fkm_cph1[idx_c, idx_p, idx_m, k - 1, idx_fold] = \
                            score_cph(cph_fkm, df_test_fkm_imputed_k, score=score)

                        cph_bpca = CoxPHFitter(penalizer=0.001).fit(df_train_bpca_imputed_k, 'Survival_time', 'Status_k')
                        score_bpca_cph1[idx_c, idx_p, idx_m, k - 1, idx_fold] = \
                            score_cph(cph_bpca, df_test_bpca_imputed_k, score=score)

    # Fit model on complete data for reference
    print('Fitting model on full data')

    for idx_fold, indexes in enumerate(cv.split(df, df.Status)):
        train_index = indexes[0]
        test_index = indexes[1]

        for k in [1, 2]:
            df_train_c = df.iloc[train_index].copy()
            df_test_c = df.iloc[test_index].copy()

            df_train_c.loc[df_train_c.Status != k, 'Status_k'] = 0
            df_train_c.loc[df_train_c.Status == k, 'Status_k'] = 1
            df_train_c.drop(columns=['Status'], inplace=True)

            df_test_c.loc[df_test_c.Status != k, 'Status_k'] = 0
            df_test_c.loc[df_test_c.Status == k, 'Status_k'] = 1
            df_test_c.drop(columns=['Status'], inplace=True)

            cph = CoxPHFitter(penalizer=0.1).fit(df_train_c, 'Survival_time', 'Status_k')
            score_full[k - 1, idx_fold] = score_cph(cph, df_test_c, score=score)

    np.save(out_path + 'score_mean_cph1_' + score, score_mean_cph1)
    np.save(out_path + 'score_fkm_cph1_' + score, score_fkm_cph1)
    np.save(out_path + 'score_bpca_cph1_' + score, score_bpca_cph1)
    np.save(out_path + 'score_full_cph1_' + score, score_full)

    return "SUCCESS"



