import argparse
from .imputers.cross_validation import plot_error, cross_validation
import os
import pandas as pd
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--fit_data", help="path for the data to fit the imputers", required=True)
parser.add_argument("-a", "--amp_data", help="path for the amputed data to fit the imputers")
parser.add_argument("-o", "--output", help="path for the mse output", required=True)
parser.add_argument("-n", "--num_splits", help="number of folds for the cross-validation", default=5, type=int)
parser.add_argument("-r", "--random_state", help="Seed for the random folds.", default=None, type=int)
parser.add_argument("-p", "--prop", help="proportion of missingness for the amputer", default=0.1, type=float)
parser.add_argument("-m", "--mech", help="mechanism of missingness for the amputer", default='MAR', type=str)
parser.add_argument("-c", "--num_centers", help="max number of cluster centers for cross-validation", default=5,
                    type=int)
parser.add_argument("-f", "--fuzziness", help="max fuzziness parameter for cross-validation. Must be bigger than 1",
                    default=3,
                    type=float)
parser.add_argument("-i", "--maxiter", help="maximum number of iterations to fit the FKM imputer", default=100,
                    type=int)
parser.add_argument("-e", "--error", help="stopping criterion to fit the FKM imputer", default=1e-5, type=float)
args = parser.parse_args()


def main():
    if args.random_state:
        np.random.seed(args.random_state)
    os.environ["R_SEED"] = str(args.random_state)
    df = pd.read_csv(args.fit_data)
    if args.amp_data is not None:
        df_amputed = pd.read_csv(args.amp_data)
    else:
        df_amputed = None
    best_params, cv_error = cross_validation(df, df_amputed=df_amputed, n_splits=args.num_splits,
                                             random_state=args.random_state,
                                             prop=args.prop, mech=args.mech, c=args.num_centers, m=args.fuzziness,
                                             maxiter=args.maxiter, max_error=args.error, shuffle=False)
    plot_error(best_params, cv_error, args.output, args.num_centers, args.fuzziness)
    print('SUCCESS !')
