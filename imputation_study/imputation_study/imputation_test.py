import argparse
from .imputers.imputer_comparison import imputation_test
import pandas as pd
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--fit_data", help="path for the data to fit the imputers")
parser.add_argument("-o", "--output", help="path for the mse output")
parser.add_argument("-t", "--table", help="path for the table of parameters", default=None)
parser.add_argument("-n", "--num_simulations", help="number of ampute/impute simulations", default=100,  type=int)
parser.add_argument("-r", "--random_state", help="Seed for the random folds.", default=None, type=int)
parser.add_argument("-p", "--prop", help="proportion of missingness for the amputer", default='0.1',  type=str)
parser.add_argument("-m", "--mech", help="mechanism of missingness for the amputer", default='MAR',  type=str)
parser.add_argument("-c", "--num_centers", help="max number of cluster centers for cross-validation", default=5,
                    type=int)
parser.add_argument("-f", "--fuzziness", help="max fuzziness parameter for cross-validation. Must be bigger than 1",
                    default=3,
                    type=float)
parser.add_argument("-i", "--maxiter", help="maximum number of iterations", default=100,  type=int)
parser.add_argument("-e", "--error", help="stopping criterion", default=1e-5,  type=float)
args = parser.parse_args()


def main():
    if args.random_state:
        np.random.seed(args.random_state)
    if args.table is not None:
        params_table = pd.read_csv(args.table)
        for index, row in params_table.iterrows():
            error_data = imputation_test(row, float(row['prop']), row['mech'])
            error_data['mech'] = row['mech']
            error_data['prop_missing'] = row['prop']
            error_data.to_csv(row['output'], index=False)
    else:
        error_data = pd.DataFrame()
        mechanisms = args.mech.split(' ')
        proportions = args.prop.split(' ')
        for mech in mechanisms:
            for prop in proportions:
                error = imputation_test(args, float(prop), mech)
                error['mech'] = mech
                error['prop_missing'] = prop
                error_data = pd.concat([error_data, error])
        error_data.to_csv(args.output, index=False)
