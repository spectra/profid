import numpy as np
from sklearn.base import TransformerMixin
from ..imputers import PandasTransformMixin


class MCARAmputerNP(TransformerMixin):
    def __init__(self, p):
        self.p = p

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        X_ = X.copy()
        mask = np.random.choice([True, False], size=[X_.shape[0], X_.shape[1]], p=self.p)
        X_[mask] = np.nan
        return X_


class MCARAmputer(PandasTransformMixin, MCARAmputerNP):
    pass