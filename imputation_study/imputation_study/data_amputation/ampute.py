import pkg_resources
import rpy2.robjects as robjects

resource_package = 'imputation_study'
resource_path_ampute = '/'.join(('data_amputation', 'ampute.r'))
ampute_file = pkg_resources.resource_filename(resource_package, resource_path_ampute)


r = robjects.r


def get_ampute():
    ampute = r.source(ampute_file)[0]
    return ampute