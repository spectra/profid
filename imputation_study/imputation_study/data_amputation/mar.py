import numpy as np
import scipy.stats
from sklearn.base import TransformerMixin
from ..imputers import PandasTransformMixin


class MARAmputerNP(TransformerMixin):
    def __init__(self, p):
        self.cov = None
        self.p = p

    def fit(self, X, y=None):
        self.cov = np.cov(X.transpose())
        return self

    def transform(self, X, y=None):
        X_ = X.copy()
        a = np.random.multivariate_normal(mean=np.zeros(X_.shape[1]), cov=self.cov, size=X_.shape[0])
        for j in range(a.shape[1]):
            mask = np.abs(a[:, j]) > scipy.stats.norm.ppf(self.p, 0, self.cov[j][j])
            X_[:, j][mask] = np.nan
        return X_


class MARAmputer(PandasTransformMixin, MARAmputerNP):
    pass