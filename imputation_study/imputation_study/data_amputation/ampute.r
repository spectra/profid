require("mice")
seed <- as.numeric(Sys.getenv("R_SEED"))
if (!is.na(seed)){
    print(seed)
    set.seed(seed)
}


ampute_custom <- function(data, prop=0.1, patterns, mech='MAR'){
    amp <- ampute(data=data, prop=prop, patterns=patterns, mech=mech)
    amp$amp
}