from .fkm_imputer import FuzzyKMeansEstimator
from .bpca_imputer import BayesianPCAEstimator
from .cross_validation import cross_validation
from .base import ampute_df
from sklearn.metrics import mean_squared_error
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler
import numpy as np
import pandas as pd
import rpy2.robjects as robjects

r = robjects.r


def imputation_test(args, prop, mech):
    df = pd.read_csv(args.fit_data)
    df = df.astype(float)
    df = pd.DataFrame(StandardScaler().fit_transform(df))
    d = df.shape[0]
    error = np.zeros([args.num_simulations, 5])
    for i in range(args.num_simulations):
        print('iteration ', i+1, ' with missing mechanism ', mech, ' and missing proportion ', prop * 100, '%')
        df_missing = pd.DataFrame(ampute_df(df, prop=prop, mech=mech))
        df_cv_complete = df.iloc[:int(4 * d / 5)]
        df_to_fit = df_missing.iloc[:int(4 * d / 5)]
        df_to_impute = df_missing.iloc[int(4 * d / 5):]
        df_test = df.iloc[int(4 * d / 5):]

        best_params, cv_error = cross_validation(df_cv_complete, df_amputed=df_to_fit, n_splits=10,
                                                 random_state=args.random_state,
                                                 prop=prop, mech=mech, c=args.num_centers, m=args.fuzziness,
                                                 maxiter=args.maxiter, max_error=args.error)

        fkm_imputer = FuzzyKMeansEstimator(c=best_params['c'], m=best_params['m'], error=args.error,
                                           maxiter=args.maxiter, seed=args.random_state).fit(df_to_fit)
        bpca_imputer = BayesianPCAEstimator(n_iteration=args.maxiter, probabilistic=False,
                                            probabilistic_inv=False).fit(df_to_fit)
        mean_imputer = SimpleImputer().fit(df_to_fit.fillna(np.nan))

        df_imputed_fkm = fkm_imputer.transform(df_to_impute)
        df_imputed_bpca = bpca_imputer.transform_imputed(X=df_to_impute, X_imp=df_imputed_fkm)
        df_imputed_mean = mean_imputer.transform(df_to_impute.fillna(np.nan))

        error[i][0] = best_params['c']
        error[i][1] = best_params['m']
        error[i][2] = mean_squared_error(df_test, df_imputed_mean)
        error[i][3] = mean_squared_error(df_test, df_imputed_fkm)
        error[i][4] = mean_squared_error(df_test, df_imputed_bpca)
    return pd.DataFrame(columns=['best_num_clusters', 'best_fuzziness',
                                 'mean_imputation', 'fkm_imputation', 'bpca_imputation'], data=error)
