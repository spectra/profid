from .fkm_imputer import FuzzyKMeansEstimator
import numpy as np
import pandas as pd
import rpy2.robjects as robjects
from rpy2.robjects import numpy2ri
from rpy2.robjects import pandas2ri
from ..data_amputation.ampute import get_ampute
from joblib import load

r = robjects.r

numpy2ri.activate()
pandas2ri.activate()
ampute = get_ampute()


def ampute_df(df, prop=None, mech=None):
    r_patterns = create_missing_pattern(df, p=0.2)
    r_dataframe = pandas2ri.py2rpy(df)
    r_amp = ampute(r_dataframe, patterns=r_patterns, prop=prop, mech=mech)
    return r_amp


def ampute_features(df, cols, prop, mech):
    patterns = np.ones(df.shape[1])
    patterns[cols] = 0
    r_patterns = r.matrix(patterns, nrow=1, ncol=df.shape[1])
    r_dataframe = pandas2ri.py2rpy(df)
    r_amp = ampute(r_dataframe, patterns=r_patterns, prop=prop, mech=mech)
    return pd.DataFrame(r_amp)


def create_missing_pattern(df, p=0.5):
    patterns = np.ones(df.shape) - np.random.choice(2, size=df.shape, p=[p, 1-p])
    nr, nc = patterns.shape
    mask = (np.sum(patterns, axis=1) == nc) | (np.sum(patterns, axis=1) == 0)
    patterns = patterns[~mask]
    patterns = patterns[0:3]
    r_patterns = r.matrix(patterns, nrow=3, ncol=nc)
    return r_patterns


def load_and_impute(args):
    # load data
    to_impute = pd.read_csv(args.impute_data)
    to_impute_noid = to_impute.drop(columns=['ID'])
    fkm = load(args.imputer) 

    # impute missing data
    df_imputed = fkm.transform(to_impute_noid)
    df_imputed = pd.concat([to_impute['ID'], df_imputed], axis=1)
    df_imputed.to_csv(args.output, index=False)

