from ..cluster_ocs import cmeans
import numpy as np
from sklearn.base import TransformerMixin
from .transforms import PandasTransformMixin
from sklearn.impute import SimpleImputer


class FuzzyKMeansEstimatorNP(TransformerMixin):
    def __init__(self, c=2, m=2, error=1e-5, maxiter=100, seed=None):
        self.c = c
        self.m = m
        self.error = error
        self.maxiter = maxiter
        self.centers = None
        self.seed = seed

    # Calculate distance between two points based on euclidean distance
    def _distance(self, data_1, data_2):
        return np.linalg.norm(data_1 - data_2)

    # Calculate the membership value for given point
    def _membership(self, dist_matrix, distance, m):
        numerator = np.power(distance, -2 / (m - 1))
        denominator = np.power(dist_matrix, -2 / (m - 1)).sum()
        return numerator / denominator

    # Extract complete and incomplete rows
    def _extract_rows(self, X):
        complete_mask = ~np.isnan(X).any(axis=1)
        missing_mask = np.logical_not(complete_mask)
        return X[complete_mask], X[missing_mask]

    def set_params(self, **parameters):
        for parameter, value in parameters.items():
            setattr(self, parameter, value)
        return self

    # Estimate the missing values
    def fit(self, X, y=None, **kwargs):
        complete_rows, incomplete_rows = self._extract_rows(X)
        centers, _, _, _, _, _, _, _ = cmeans(data=complete_rows, c=self.c, m=self.m, error=self.error,
                                              maxiter=self.maxiter, init=None, seed=self.seed)
        self.centers = centers

        return self

    def transform(self, X, y=None):
        X_ = X.copy()
        X_[np.isnan(X_).all(axis=1)] = self.centers.mean(axis=0) # impute simple average of cluster centers when for fully empty rows
        miss_ind_data = np.argwhere(np.isnan(X_).any(axis=1))
        for idx in miss_ind_data:
            x_i = X_[idx[0]]
            dist = []
            miss_ind = np.argwhere(np.isnan(x_i))

            for center in self.centers:
                dist.append(self._distance(data_1=np.delete(center, miss_ind),
                                           data_2=np.delete(x_i, miss_ind)))

            if 0 in dist:
                # This happens if all features for one row are missing or a row is equal to one of the centers
                c_idx = np.argwhere(np.array(dist) == 0)[:, 0]
                x_i[miss_ind] = np.mean(self.centers[c_idx][:, miss_ind], axis=0)

            else:
                estimated = 0
                membership_value = []
                for d in dist:
                    membership_value.append(self._membership(np.array(dist), d, self.m))

                for k in range(self.c):
                    estimated += self.centers[k][miss_ind] * membership_value[k]
                x_i[miss_ind] = estimated

        return X_


class FuzzyKMeansEstimator(PandasTransformMixin, FuzzyKMeansEstimatorNP):
    pass


class MeanImputerNP(SimpleImputer):
    def transform(self, X, y=None):
        return super().transform(X)


class MeanImputer(PandasTransformMixin, MeanImputerNP):
    pass