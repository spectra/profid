from sklearn.metrics import mean_squared_error
from sklearn.model_selection import ParameterGrid, KFold, StratifiedKFold, cross_val_score
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
import pandas as pd
import numpy as np
from .fkm_imputer import FuzzyKMeansEstimator
import matplotlib.pyplot as plt
from .base import ampute_df
from hyperopt import fmin, tpe, hp, Trials
from lifelines.utils.sklearn_adapter import sklearn_adapter
from lifelines import CoxPHFitter
from joblib import dump
from functools import partial


def cross_validation(df, df_amputed=None, n_splits=5, random_state=None, prop=None, mech=None, c=6, m=3, m_step=0.1,
                     maxiter=None, max_error=None, shuffle=False):
    data = df.astype(float)
    if df_amputed is None:
        data = df[~df.isna().any(axis=1)].reset_index(drop=True)
        data = pd.DataFrame(StandardScaler().fit_transform(data))
        data_amputed = pd.DataFrame(ampute_df(data, prop=prop, mech=mech))
    else:
        data_amputed = df_amputed
    param_dict = {
        'c': range(2, c+1),
        'm': np.arange(1.1, m+m_step, m_step)
    }
    cv_seed = random_state if shuffle else None
    kf = KFold(n_splits=n_splits, random_state=cv_seed, shuffle=shuffle)
    error = []
    for params in list(ParameterGrid(param_dict)):
        fkm = FuzzyKMeansEstimator(c=params['c'], m=params['m'], maxiter=maxiter, error=max_error, seed=random_state)
        err = 0
        for train_index, test_index in kf.split(data):
            train = data_amputed.iloc[train_index]
            test_amputed = data_amputed.iloc[test_index]
            test = data.iloc[test_index]

            fkm = fkm.fit(train)
            test_imputed = fkm.transform(test_amputed)
            err = err + mean_squared_error(test_imputed, test)
        error.append([params['c'], params['m'], err / 5])
    error = np.array(error)
    im = np.argmin(error[:, 2])
    return {'c': int(error[im][0]), 'm': error[im][1]}, error


def get_score(X, Y, seed, hyperparams):
    params = {'c': int(hyperparams['c']), 
              'm': hyperparams['m']}
    cv = StratifiedKFold(n_splits=5, shuffle=True, random_state=seed)
    scores = []
    
    CoxRegression = sklearn_adapter(CoxPHFitter, event_col='Status')
    pipe = Pipeline(steps=[('fkm', FuzzyKMeansEstimator(**params)), ('cph', CoxRegression(penalizer=0.001))])
    
    score = cross_val_score(pipe, X, Y, cv=cv.split(X, X.Status)).mean()

    return score


def get_best_params(fit_data, seed=None):
    space={'c': hp.randint('c', 2, 10),
           'm': hp.uniform('m', 1.1, 3)
    }

    # trials will contain logging information
    trials = Trials()
    X = fit_data.drop('Survival_time', axis=1)
    Y = fit_data['Survival_time']
    X.loc[X.Status != 1, 'Status'] = 0
    objective = partial(get_score, X, Y, seed)

    best=fmin(fn=objective, 
            space=space, 
            algo=tpe.suggest, 
            max_evals=10, 
            trials=trials, 
            rstate=np.random.RandomState(seed) # fixing random state for reproducibility
        )
    
    return best


def save_imputer(data, out_path, seed=None):
    fit_data = pd.read_csv(data)
    fit_data = fit_data.drop(columns=['ID'])
    best = get_best_params(fit_data, seed=None)
    fkm_imputer = FuzzyKMeansEstimator(**best).fit(fit_data)
    dump(fkm_imputer, out_path + '.joblib')


def plot_error(best_params, error, path, c, m, m_step=0.1):
    c_vect = range(2, c+1)
    m_vect = np.arange(1.1, m+m_step, m_step)
    error_flip = error[::-1]
    Z = np.reshape(error_flip[:, 2], (c-1, m_vect.size))[:, ::-1]
    plt.yticks(c_vect)
    plt.ylabel('n_clusters')
    plt.xlabel('fuzziness')
    plt.title('Best score for n_clusters=' + str(best_params['c']) + ' and fuzziness=' + str("{0:.2f}".format(best_params['m'])))
    plt.imshow(Z, extent=(np.amin(m_vect), np.amax(m_vect), np.amin(c_vect), np.amax(c_vect)), aspect='auto')
    plt.colorbar()
    plt.savefig(path, bbox_inches='tight')
