import os
from .imputation_comparison import *
from .imputation_regions import *
from .import_data import *


def get_imputation_scores(protocol=1, filename=None, filename_regions=None, prop=None, mech=None, seed=None,
                          score=None, out_path=None):

    if protocol == 1:
        os.environ["R_SEED"] = str(seed)
        df, rc_a = import_data(filename=filename)

        imputation_cph_comparison(df, rc_a, prop=prop, mech=mech, seed=seed, score=score,
                                  out_path=out_path)
    elif protocol == 2:

        fr_df, rc_fr = import_french_data(filename=filename, regions=True, filename_regions=filename_regions)

        imputation_cph_regions(df=fr_df, common_cols_base_types=rc_fr, seed=seed, score=score, out_path=out_path)