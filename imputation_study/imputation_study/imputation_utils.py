import numpy as np
import pandas as pd
from sksurv.metrics import concordance_index_ipcw, integrated_brier_score as integrated_brier_score_ipcw
from sksurv.util import Surv
from lifelines.utils import concordance_index as concordance_index_lifelines
from missing_value_imputation.imputers import FuzzyKMeansEstimator, BayesianPCAEstimator


def make_surv_data(df, t_factor=100, base_rate=1, shape=1, hr_1a=1.15, hr_1b=1.11, hr_2a=1.15, hr_2b=1.11,
                   predictor_a='Age', predictor_b='Ejection_fraction'):
    df_simple = df.copy()
    v = np.random.rand(df_simple.shape[0])
    t = (- np.log(v) / (base_rate * (np.exp(np.log(hr_1a) * df_simple[predictor_a].values +
                                            np.log(hr_1b) * df_simple[predictor_b].values) +
                                     np.exp(np.log(hr_2a) * df_simple[predictor_a].values +
                                            np.log(hr_2b) * df_simple[predictor_b].values)))) ** (1 / shape)
    df_simple['Survival_time'] = t_factor * t
    p = np.exp(np.log(hr_1a) * df_simple[predictor_a].values + np.log(hr_1b) * df_simple[predictor_b].values) / \
        (np.exp(np.log(hr_1a) * df_simple[predictor_a].values + np.log(hr_1b) * df_simple[predictor_b].values) +
         np.exp(np.log(hr_2a) * df_simple[predictor_a].values + np.log(hr_2b) * df_simple[predictor_b].values))
    status = np.random.binomial(n=1, p=p, size=df_simple.shape[0])
    df_simple['Status'] = (2 - status).astype(int)
    return df_simple


def get_matching_cols(df, col, rc_columns_types):
    idx = 0
    for c in rc_columns_types.keys():
        if c != col:
            idx = idx + rc_columns_types[c]['nb_cat']
        else:
            break
    return df.columns[idx:idx + rc_columns_types[col]['nb_cat']]


def fit_imputers_cph(df_amputed, indexes, common_cols_base_types, current_col, matching_cols,
                     n_centers=3, fuzziness=2, n_iteration=10):
    fkm = FuzzyKMeansEstimator(c=n_centers, m=fuzziness)
    bpca = BayesianPCAEstimator(n_iteration=n_iteration)

    train_index = indexes[0]
    test_index = indexes[1]

    df_train_c = df_amputed.iloc[train_index].copy()
    df_test_c = df_amputed.iloc[test_index].copy()

    if 'Implant_centre' in df_amputed.columns:
        X_train = df_train_c.drop(columns=['Survival_time', 'Status', 'Implant_centre'])
        X_test = df_test_c.drop(columns=['Survival_time', 'Status', 'Implant_centre'])
    else:
        X_train = df_train_c.drop(columns=['Survival_time', 'Status'])
        X_test = df_test_c.drop(columns=['Survival_time', 'Status'])
    
    X_train_mean_imputed = X_train.fillna(X_train.mean())
    X_test_mean_imputed = X_test.fillna(X_train.mean())
    
    X_train_fkm_imputed = fkm.fit_transform(X_train)
    X_test_fkm_imputed = fkm.transform(X_test)
    
    X_train_bpca_imputed = bpca.fit_transform(X_train)
    X_test_bpca_imputed = bpca.transform(X_test)
    
    if matching_cols[0] in df_amputed.columns and common_cols_base_types[current_col]['is_categorical']:
        X_train_mean_imputed, X_test_mean_imputed = \
            rc_cph(X_train_mean_imputed, X_test_mean_imputed, matching_cols)

        X_train_fkm_imputed, X_test_fkm_imputed = \
            rc_cph(X_train_fkm_imputed, X_test_fkm_imputed, matching_cols)

        X_train_bpca_imputed, X_test_bpca_imputed = \
            rc_cph(X_train_bpca_imputed, X_test_bpca_imputed, matching_cols)

    df_train_mean_imputed = pd.concat([X_train_mean_imputed,
                                       df_train_c[['Survival_time', 'Status']]], axis=1)

    df_test_mean_imputed = pd.concat([X_test_mean_imputed,
                                      df_test_c[['Survival_time', 'Status']]], axis=1)

    df_train_fkm_imputed = pd.concat([X_train_fkm_imputed,
                                      df_train_c[['Survival_time', 'Status']]], axis=1)

    df_test_fkm_imputed = pd.concat([X_test_fkm_imputed,
                                     df_test_c[['Survival_time', 'Status']]], axis=1)

    df_train_bpca_imputed = pd.concat([X_train_bpca_imputed,
                                       df_train_c[['Survival_time', 'Status']]], axis=1)

    df_test_bpca_imputed = pd.concat([X_test_bpca_imputed,
                                      df_test_c[['Survival_time', 'Status']]], axis=1)

    return df_train_mean_imputed, df_test_mean_imputed, df_train_fkm_imputed, df_test_fkm_imputed, df_train_bpca_imputed, df_test_bpca_imputed


def censor_competing_risk(df_train, df_test, k):
    df_train_c = df_train.copy()
    df_test_c = df_test.copy()

    df_train_c.loc[df_train_c.Status != k, 'Status_k'] = 0
    df_train_c.loc[df_train_c.Status == k, 'Status_k'] = 1

    df_test_c.loc[df_test_c.Status != k, 'Status_k'] = 0
    df_test_c.loc[df_test_c.Status == k, 'Status_k'] = 1

    df_train_c.drop(columns=['Status'], inplace=True)
    df_test_c.drop(columns=['Status'], inplace=True)

    return df_train_c, df_test_c


def rc_cph(df_train_imputed, df_test_imputed, matching_cols):
    max_c = df_train_imputed[matching_cols].max(axis=1)
    for col in matching_cols:
        df_train_imputed[col] = (df_train_imputed[col] == max_c).astype(int)

    max_c = df_test_imputed[matching_cols].max(axis=1)
    for col in matching_cols:
        df_test_imputed[col] = (df_test_imputed[col] == max_c).astype(int)
    return df_train_imputed, df_test_imputed


def score_cph(model, df, time=None, score='concordance_l'):
    if score == 'brier_isw':
        t_cens_test_max = df[df.Status_k == 0].Survival_time.max()
        df_trunc = df[(df.Status_k == 0) | ((df.Status_k == 1) & (df.Survival_time <= t_cens_test_max))].reset_index()
        t_min = df_trunc[df_trunc.Status_k == 1].Survival_time.min()
        if time is None:
            t_max = np.floor(df_trunc[df_trunc.Status_k == 1].Survival_time.max())
        else:
            t_max = time
        survival_times = np.arange(t_min, t_max)
        pred = np.transpose([model.predict_survival_function(df_trunc, t).values for t in survival_times])[:, 0, :]
        y_test = Surv.from_arrays(df_trunc.Status_k == 1, df_trunc.Survival_time)
        return integrated_brier_score_ipcw(y_test, y_test, pred, survival_times)
    elif score == 'concordance_l':
        return concordance_index_lifelines(df['Survival_time'], -model.predict_partial_hazard(df), df['Status_k'])
    elif score == 'concordance_sw':
        pred = - model.predict_partial_hazard(df).values
        y_test = Surv.from_arrays(df.Status_k, df.Survival_time)
        return concordance_index_ipcw(y_test, y_test, pred)[0]

