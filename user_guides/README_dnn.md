First make sure, you have Python 3 installed on your computer. To create a virtual environment in the package folder, run the command:

    python3 -m venv env
    
Then, activate this virtual environment

    source env/bin/activate

If you already have installed a previous version of the imputer in your virtual environment and want to update it, uninstall the current version first:

    pip uninstall profid_risk_model

Then, inside the 'profid_risk_model' folder, you can install the DNN package with the command:
    
    python setup.py install

You are now able to use the dnn_fit and dnn_predict commands. 


Here is the structure of the dnn_fit command:

    dnn_fit -d <filename> -o <directory path> -v <integer> -r <integer>
    
The parameters for dnn_fit are described below:

    "-d" or "--data": "data path to fit the model (needs to be a csv file)"
    "-o" or "--output": "directory path for to save the fitted neural network model and scaler""
    "-v" or "--variable_cap": "maximum number of best variables to be selected. If -1 or not specified, no capping is applied."
    "-r" or "--random_state": "optional: seed used for hyper parameter optimization"


Here is the structure of the dnn_predict command:

    dnn_predict -d <filename> -m <directory path> -t <filename> -o <filename>
    
The parameters for dnn_predict are described below:

    "-d" or "--data": "data path to get predictions from (needs to be a csv file)"
    "-m" or "--model": "directory path for a fitted neural network model and scaler""
    "-t" or "--times": "path for a file containing evaluation times and patient IDs for prediction (needs to be a csv file)"
    "-o" or "--output": "output path to save predictions (needs to be a csv file)"
 


    