First make sure, you have Python 3 installed on your computer. To create a virtual environment in the package folder, run the command:

    python3 -m venv env
    
Then, activate this virtual environment

    source env/bin/activate

Then, inside the 'fuzzy_kmeans_poc' folder, you can install the required packages with the command:

    pip install -r requirements.txt

Still inside the 'fuzzy_kmeans_poc' folder you can then run the example script. This script is a 'sanity check' that creates a very simple Pandas dataframes with rows made with 
features all equal to 0 or 1, and then randomly removes some of the features. The imputer should normally 
fill missing zeros with values close to 0 and missing ones with values close to 1.
 
    python scripts/example.py
    

If there is an error preventing from running the script try running this command first

    python setup.py install