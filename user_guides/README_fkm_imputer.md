First make sure, you have Python 3 installed on your computer. To create a virtual environment in the package folder, run the command:

    python3 -m venv env
    
Then, activate this virtual environment

    source env/bin/activate

If you already have installed a previous version of the imputer and want to update it, uninstall the current version first:

    pip uninstall missing_value_imputation

Then, inside the 'missing_value_imputation' folder, you can install the imputer package with the command:
    
    python setup.py install

You are now able to use the fkm_fit and fkm_impute commands. 

Here is the structure of the fkm_fit command:

    fkm_fit -f <filename> -o <filename> -r <integer>
    
    
The parameters for fkm_fit are described below:

    "-f" or "--fit_data": "data to fit the imputer (needs to be a csv file)"
    "-o" or "--output": "output path to save imputer (needs to be a json file)"
    "-r" or "--random_state": "optional: seed used for hyper parameter optimization"

Here is the structure of the fkm_impute command:

    fkm_impute -d <filename> -o <filename> -i <filename> 
    
    
The parameters for fkm_impute are described below:

    "-d" or "--impute_data": "data to be imputed (needs to be a csv file)"
    "-o" or "--output": "output path to save imputed data (needs to be a csv file)"
    "-i" or "--imputer": "path to load a fitted imputer"
    
The number of clusters and fuzziness are optimized with cross-validation.
 


    