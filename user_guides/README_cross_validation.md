First make sure you have Python 3 installed on your computer. To create a virtual environment in the package folder, run the command:

    python3 -m venv env
    
Then, activate this virtual environment

    source env/bin/activate

Then, inside the 'missing_value_imputation' folder, you can install the package with the command:

    python setup.py install

You are now able to use the cross_validate command. 
Here is the structure of the command:

    cross_validate -d <filename> -o <filename>  -n <value> -p <string> -m <string> -c <value> -f <value> -i <value> -e <value> 
    
This command runs a cross-validation of the FKM imputer on the provided data, to find an optimal combination of
values for the number of clusters and the fuzziness. The data is standardized before the cross-validation.
The output is an plot of the MSE for the whole grid of parameters the cross-validation has been run on. 
The title mentions the best choice of parameters. This plot will be generated using the full path specified 
by the -o parameter, for instance:
    
    '/home/data/cross_validation_plot.pdf'
  
The parameters are described below:

    "-d" or "--fit_data": "path for the data to fit the imputers"
    "-o" or "--output": "path for the mse output"
    "-n" or "--num_splits": "number of folds for the cross-validation"
    "-r" or "--random_state": "Seed for the random amputing and folds."
    "-p" or "--prop": "proportion of missingness for the amputer"
    "-m" or "--mech": "mechanism of missingness for the amputer"
    "-c" or "--num_centers"; "max number of cluster centers for cross-validation"
    "-f" or "--fuzziness": "max fuzziness parameter for cross-validatio. Must be bigger than 1"
    "-i" or "--maxiter": "maximum number of iterations to fit the FKM imputer"
    "-e" or "--error": "maximum error criterion to fit the FKM imputer"
    
The following  parameters have default values:
     
     n=5, r=None, p='0.1', m='MAR', c=5, m=3, i=100 and e=1e-5.
     
From the values of parameters **m** and **c**, the search grid for the fuzziness will be [1.1, m] with a step of 0.1 
and the one for the number of clusters will be [1, c] with a step of 1.
 
At the very least the fitting data (parameter -d) and output plot path (parameter -o) should be provided.

If the missing data contains missing values, the corresponding rows will be removed for the cross-validation.

The user can choose to provide a seed (random state). If this is the case there will be a random shuffling of the data
using this seed when creating the F-folds for the cross-validation. Otherwise the data will not be shuffled.





    