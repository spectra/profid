First make sure you have Python 3 installed on your computer. To create a virtual environment in the package folder, run the command:

    python3 -m venv env
    
Then, activate this virtual environment

    source env/bin/activate

Then, inside the 'missing_value_imputation' folder, you can install the imputer package with the command:

    python setup.py install

You are now able to use the imputation_test command. 
Here is the structure of the command:

    imputation_test -d <filename> -o <filename>  -n <value> -p <string> -m <string> -c <value> -f <value> -i <value> -e <value> 
    
This command runs amputation scenarios on the provided data, then fits mean, FKM and Bayesian PCA
on 80% of the amputed data then the imputation mean squared error is calculated on the remaining 20%.
This command also runs a cross-validation of the FKM imputer on each iteration of the amputed data
to find an optimal combination of values for the number of clusters and the fuzziness. 
The data is standardized before the cross-validation. In order to use multiple mechanisms and proportions
of missingness, the corresponding inputs should be strings with spaces,
for instance 'MNAR MAR' and '0.1 0.55 0.9' respectively.

The output will be generated using the full path specified by the -o parameter, for instance:
'/home/data/imputation_report.csv'
  
The parameters are described below:

    "-d" or "--fit_data": "path for the data to fit the imputers"
    "-o" or "--output": "path for the mse output"
    "-n" or "--num_simulations": "number of ampute/impute simulations"
    "-r" or "--random_state": "Seed for the random amputing and folds."
    "-p" or "--prop": "list of proportions of missingness for the amputer"
    "-m" or "--mech": "mechanism of missingness for the amputer"
    "-c" or "--num_centers"; "max number of cluster centers for cross-validation"
    "-f" or "--fuzziness": "fuzziness parameter for the FKM imputer. Must be bigger than 1"
    "-i" or "--maxiter": "maximum number of iterations when fitting the imputers"
    "-e" or "--error": "maximum error criterion when fitting the imputers"
    
The following  parameters have default values:
     
     n=10, r=None, p='0.1', m='MAR', c=5, m=3, i=10 and e=1e-3.
 
From the values of parameters **m** and **c**, the search grid for the fuzziness will be [1.1, m] with a step of 0.1 
and the one for the number of clusters will be [1, c] with a step of 1.

At the very least the fitting data (parameter -d) and output data path (parameter -o) should be provided.
The FKM imputer will separate complete and incomplete rows in the fitting data, use the complete
rows to calculate the centers then impute the incomplete rows.

The user can choose to provide a seed (random state). If this is the case there will be a random shuffling of the data
using this seed when creating the F-folds for the cross-validation. Otherwise the data will not be shuffled.





    