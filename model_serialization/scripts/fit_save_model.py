import sys
from model_serialization.model_to_json import process


model_name = sys.argv[1]


if __name__ == "__main__":
    process(name=model_name)
