from keras import Sequential
from keras.layers import Dense
import numpy as np
import os


def train_test_split(X, y):
    N = X.shape[0]
    split_size = int(N / 5)
    split = int(N - 2 * split_size)
    train_X = X[:split]
    train_y = y[:split]
    val_X = X[split:split + split_size]
    val_y = y[split:split + split_size]
    test_X = X[split + split_size:]
    test_y = y[split + split_size:]

    return train_X, train_y, val_X, val_y, test_X, test_y


def make_data(N):
    x = np.linspace(1, N, N)
    y = x + np.random.randn(N)*200
    return train_test_split(x, y)


def train_model(train_X, train_y):
    model = Sequential()
    model.add(Dense(20, input_shape=(1,)))
    model.add(Dense(30, activation='relu'))
    model.add(Dense(30, activation='relu'))
    model.add(Dense(1))
    model.compile(loss='mse', optimizer='adam')

    history = model.fit(train_X, train_y, epochs=5, validation_split=0.4, batch_size=10, verbose=0)
    return model


def process(name, N=10000):

    train_X, train_y, val_X, val_y, test_X, test_y = make_data(N)
    model = train_model(train_X, train_y)
    model.save('{}.h5'.format(name))
    os.system("h5tojson {}.h5 > {}.json".format(name, name))
