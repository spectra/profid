from setuptools import setup, find_packages


setup(
    name='profid_risk_model',
    version='0.2.0',
    packages=find_packages(),
    install_requires=[
    "pandas==0.25.3",
    "numpy==1.19.5",
    "scikit_learn==0.22.1",
    "lifelines==0.25.11",
    "hyperopt==0.2.5",
    "scikit-survival==0.13.1",
    "tensorflow==2.5.0"],
    zip_safe=False,
    include_package_data=True,
    entry_points={
        'console_scripts': ['dnn_fit=profid_risk_model.main:fit',
                            'dnn_predict=profid_risk_model.main:predict']
    },
    python_requires='>=3.7'
)
