import argparse
import logging
from distutils import util
from profid_risk_model.deephit.process import save_model, load_and_predict

def str2bool(v):
    return bool(util.strtobool(v))

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--data", help="data path for training or prediction", required=True)
parser.add_argument("-m", "--model", help="directory path for a fitted neural network model and scaler")
parser.add_argument("-t", "--times", help="unique evaluation times for prediction", default='12',  type=str)
parser.add_argument("-o", "--output", help="output directory path to save model files or file path to save predictions", required=True)
parser.add_argument("-p", "--n_permutations", help="number of permutations for feature importance calculation", default=10, type=int)
parser.add_argument("-r", "--random_state", help="seed for the cross-validation", default=None, type=int)
parser.add_argument("-c", "--convert_to_days", help="convert months to days", nargs='?', const=True, default=True, type=str2bool)
parser.add_argument("-v", "--variable_cap", help="Maximum number of best variables to be selected", default=-1, type=int)
parser.add_argument("-s", "--variable_selection", help="allow for variable selection while fitting a model", default=False, type=str2bool)
args = parser.parse_args()


def fit():
    logging.basicConfig(filename=args.output.split('.json')[0] + '/fit_dnn.log', filemode='w', level=logging.WARNING)
    save_model(data_path=args.data, out_path=args.output, n_permutations=args.n_permutations, convert_to_days=args.convert_to_days,
     seed=args.random_state, max_m=args.variable_cap, variable_selection=args.variable_selection)
    print('SUCCESS !')


def predict():
    logging.basicConfig(filename=args.output.split('.csv')[0] + '_predict_dnn.log', filemode='w', level=logging.WARNING)
    load_and_predict(data_path=args.data, model_path=args.model, eval_times=args.times, out_path=args.output, convert_to_days=args.convert_to_days)
    print('SUCCESS !')

