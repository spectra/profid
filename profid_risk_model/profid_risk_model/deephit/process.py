from sklearn.preprocessing import StandardScaler
import json
import logging, os
import pandas as pd
import numpy as np
import tensorflow as tf
from tensorflow.keras.models import Model
from hyperopt import hp, space_eval
from hyperopt.pyll.base import scope
from profid_risk_model.cross_validation import CrossValidator


tf.config.threading.set_intra_op_parallelism_threads(8)
tf.config.threading.set_inter_op_parallelism_threads(8)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

mtd = 30.436875 # for months to days conversion


def import_data(filename=None, convert_to_days=False):
    data = pd.read_csv(filename)
    df = pd.concat([data['ID'], data.drop(columns=['ID', 'Survival_time', 'Status']),
                    data[['Survival_time', 'Status']]], axis=1)
    if convert_to_days:
        print('Converting month values to days')
        df.Survival_time = df.Survival_time*mtd
    df.Survival_time = df.Survival_time.astype(int)
    df = df.reset_index(drop=True)
    return df


def get_search_space(fit_data, max_m, variable_selection):
    
    space = {
                'num_layers_shared': scope.int(hp.quniform('num_layers_shared', 1, 3, 1)),
                'num_layers_cs': scope.int(hp.quniform('num_layers_cs', 1, 3, 1)),
                'h_dim_shared': hp.choice('h_dim_shared', [50, 100]),
                'h_dim_cs': hp.choice('h_dim_cs', [50, 100])
            }

    if variable_selection or max_m != -1:
        n_features = fit_data.shape[1]-2 
        if max_m == -1:
            m_array = [max(int(n_features/10), 1), max(int(n_features/5), 1), max(int(n_features/2), 1), max(int(n_features*0.8), 1), n_features]
        elif max_m >= 1:
            m_array = [max(max_m-5, 1), max(max_m-4, 1), max(max_m-3, 1), max(max_m-2, 1), max(max_m-1, 1), max_m]
        elif max_m < -1 or max_m == 0:
            raise ValueError('Invalid value for the variable capping')
                
        space['m'] = hp.choice('m', m_array)
    return space



def cumulative_incidence_function(pred_prob, t):
    z = int(t) + 1
    return np.sum(pred_prob[:, :, :z], axis=2)


def check_time(t, model, convert_to_days):
    if convert_to_days:
        t_pred = t*mtd
    else:
        t_pred = t
    if t_pred >= model.output_shape[2]:
        err = "Time point {} not in training set".format(t)
        logging.critical(err)
        raise ValueError(err)
    return t_pred


def get_predictions(X, model, times, m, q, r, convert_to_days):
    preds = []
    for i in range(max(q+1, 1)):
        if i<q or (i==q and q > 0 and r > 0) or (q==0 and r>0):
            pred = model.predict(X.iloc[i*m:(i+1)*m, 1:].values)
            ids = X[i*m:(i+1)*m].reset_index()['ID']
        else:
            break
        for t in times['Time']:
            t_pred = check_time(t, model, convert_to_days)
            cif = cumulative_incidence_function(pred, t_pred)
            d = pd.DataFrame(columns=['ID', 'Event', 'Time', 'CIF'])
            d['CIF'] = cif.transpose().reshape(-1)
            d['ID'] = pd.concat([ids]*model.output_shape[1], ignore_index=True)
            d['Event'] = pd.concat([pd.Series([k+1]*len(ids)) for k in range(model.output_shape[1])], ignore_index=True)
            d['Time'] = t

            preds.append(d)

    df = pd.concat(preds)
    df['CHF'] = -np.log(1-df['CIF'])

    df.sort_values(['ID', 'Event', 'Time'], inplace=True)
    df.reset_index(inplace=True, drop=True)
    return df


def save_model(data_path, out_path, n_permutations=5, seed=None, convert_to_days=False, max_m=-1, variable_selection=True):
    fit_data = import_data(data_path, convert_to_days)

    fit_data.drop(columns=['ID'], inplace=True)
    
    if variable_selection or max_m != -1:
        print('Fitting with variable selection...')
    else:
        print('Fitting without variable selection...')
    space = get_search_space(fit_data, max_m, variable_selection)

    cv = CrossValidator(space=space, search_mode='hyperopt', random_state=seed, max_iter=15, n_permutations=n_permutations)
    cv.validate(fit_data, k=1)

    logging.warning('Input data dimension (without ID) is: {}'.format(fit_data.shape))
    logging.warning('Best parameters:')
    logging.warning(space_eval(space, cv.hyperopt_best_params))
    logging.warning('There as been {} selected variables out of {} total variables'.format(len(cv.best_features), fit_data.shape[1]-2))
    logging.warning('Selected variables:')
    logging.warning(cv.best_features)
    logging.warning('Training integrated Brier score with best model is: {}'.format(cv.val_error))

    df_bf = pd.DataFrame(data={'Selected_variables': cv.best_features,
                               'Variable_importance': cv.best_scores})
    df_bf.to_csv(out_path + '/best_variables.csv', index=False)

    scaler = StandardScaler().fit(fit_data[cv.best_features])
    scaler_data = {'means': scaler.mean_.tolist(),'scales':scaler.scale_.tolist(), 'in_columns': cv.best_features}
    with open(out_path + '/scaler.json', 'w') as fp:
        json.dump(scaler_data, fp)

    
    cv_dnn = cv.best_model.named_steps['dnn']
    model_data = {'config': cv_dnn.out_model.get_config(), 'weights': [w.tolist() for w in cv_dnn.out_model.get_weights()],
                  'params': cv_dnn.get_params(), 'selected_variables': cv.best_features}
    with open(out_path + '/model_data.json', 'w') as fp:
        json.dump(model_data, fp)


def load_and_predict(data_path, model_path, eval_times, out_path, convert_to_days=False):
    pred_data = import_data(data_path, convert_to_days)
    pred_data.drop(columns=['Survival_time', 'Status'], inplace=True)
    with open(model_path + '/scaler.json', 'r') as fs:
        scaler_data = json.load(fs)
    if not set(scaler_data['in_columns']) <= set(pred_data.columns.to_list()[1:]):
        err = 'Variable names are not matching with scaler'
        logging.critical(err)
        raise ValueError(err)
    scaler = StandardScaler()
    scaler.mean_ = scaler_data['means']
    scaler.scale_ = scaler_data['scales']

    with open(model_path + '/model_data.json', 'r') as fp:
        model_data = json.load(fp)
    model = Model.from_config(model_data['config'])
    model.set_weights([np.array(w) for w in model_data['weights']]) 
    
    X = scaler.transform(pred_data[scaler_data['in_columns']])    
    pred_data = pd.concat([pred_data['ID'], pd.DataFrame(columns=scaler_data['in_columns'], data=X)], axis=1)
    m = 1000
    q = int(pred_data.shape[0]/m)
    r = np.mod(pred_data.shape[0], m)
    
    
    prediction_times = pd.read_csv(eval_times)
    predictions= get_predictions(pred_data, model, prediction_times, m, q, r, convert_to_days)

    predictions.to_csv(out_path, index=False)
