'''
First implemented: 01/25/2018
  > For survival analysis on longitudinal dataset
By CHANGHEE LEE

Modifcation List:
	- 08/07/2018: weight regularization for FC_NET is added
    - 26/05/2021: conversion to TensorFlow 2 (Youssef Taleb)
'''

import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Dropout, Concatenate


### FEEDFORWARD NETWORK
def create_FCNet(input, num_layers, h_dim, h_fn, o_dim, o_fn, w_init=None, keep_prob=1.0, w_reg=None, extra_input=None, name='FCNet'):
    '''
        GOAL             : Create FC network with different specifications 
        inputs (tensor)  : input tensor
        num_layers       : number of layers in FCNet
        h_dim  (int)     : number of hidden units
        h_fn             : activation function for hidden layers (default: tf.nn.relu)
        o_dim  (int)     : number of output units
        o_fn             : activation function for output layers (defalut: None)
        w_init           : initialization for weight matrix (defalut: Xavier)
        keep_prob        : keep probabilty [0, 1]  (if None, dropout is not employed)
    '''
    # default active functions (hidden: relu, out: None)
    if h_fn is None:
        h_fn = 'relu'
    if o_fn is None:
        o_fn = None

    # default initialization functions (weight: Xavier, bias: None)
    if w_init is None:
        w_init = tf.initializers.GlorotUniform() # Xavier initialization

    h = input
    if extra_input is not None:
        h = Concatenate()([extra_input, h]) #for residual connection

    if num_layers == 1:
        out = Dense(o_dim, activation=o_fn, kernel_initializer=w_init, kernel_regularizer=w_reg, name=name + '_output')(h)
        return Model(input, out, name=name)
    else:
        for layer in range(num_layers):
            if layer == 0:
                h = Dense(h_dim, activation=h_fn, kernel_initializer=w_init, kernel_regularizer=w_reg, name=name + '_hidden_' + str(layer))(h)
                if not keep_prob is None:
                    h = Dropout(rate=1-keep_prob, name=name + '_dropout_' + str(layer))(h)

            elif layer > 0 and layer != (num_layers-1): # layer > 0:
                h = Dense(h_dim, activation=h_fn, kernel_initializer=w_init, kernel_regularizer=w_reg, name=name + '_hidden_' + str(layer))(h)
                if not keep_prob is None:
                    h = Dropout(rate=1-keep_prob, name=name + '_dropout_' + str(layer))(h)

            else: # layer == num_layers-1 (the last layer)
                out = Dense(o_dim, activation=o_fn, kernel_initializer=w_init, kernel_regularizer=w_reg, name=name + '_output')(h)

        return Model(input, out, name=name)