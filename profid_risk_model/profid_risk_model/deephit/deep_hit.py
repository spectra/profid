"""

Lee et al. - 2018 - DeepHit A deep learning approach to survival analysis with competing risks

This declare DeepHit architecture:

INPUTS:
    - input_dims: dictionary of dimension information
        > n_risks: number of competing events (this does not include censoring label)
        > time_horizon: dimension of time horizon of interest, i.e., |T| where T = {0, 1, ..., T_max-1}
                      : this is equivalent to the output dimension
    - network_settings:
        > h_dim_shared & num_layers_shared: number of nodes and number of fully-connected layers for the shared subnetwork
        > h_dim_cs & num_layers_cs: number of nodes and number of fully-connected layers for the cause-specific subnetworks
        > active_fn: 'relu', 'elu', 'tanh'
        > initial_W: Xavier initialization is used as a baseline

LOSS FUNCTIONS:
    - 1. loglikelihood (this includes log-likelihood of subjects who are censored)
    - 2. rankding loss (this is calculated only for acceptable pairs; see the paper for the definition)
    - 3. calibration loss (this is to reduce the calibration loss; this is not included in the paper version)


Modification List:
    - 26/05/2021: conversion to TensorFlow 2 and scikit-learn wrapping (Youssef Taleb)
"""

import numpy as np
from sklearn.base import BaseEstimator, RegressorMixin
from scipy import integrate
import tensorflow as tf
from tensorflow.keras.layers import Dense, Dropout, Reshape, Concatenate, Input
from tensorflow.keras.models import Model
from .utils_network import create_FCNet
from .utils_eval import weighted_brier_score


try:
    from tensorflow.python.util import module_wrapper as deprecation
except ImportError:
    from tensorflow.python.util import deprecation_wrapper as deprecation
deprecation._PER_MODULE_WARNING_LIMIT = 0

tf.get_logger().setLevel('WARNING')

_EPSILON = 1e-08


# USER-DEFINED FUNCTIONS
def log(x):
    return tf.math.log(x + _EPSILON)


def div(x, y):
    return tf.math.divide(x, (y + _EPSILON))


class DeepHit(BaseEstimator, RegressorMixin):
    def __init__(self, name='DeepHit', n_risks=1, time_horizon=100, n_epochs=None, delta=0.005, max_patience=5,
                 mb_size=None, h_dim_shared=50, h_dim_cs=50, num_layers_shared=1, num_layers_cs=1,
                 keep_prob=1.0, lr_train=1e-3, active_fn='relu', alpha=1.0, beta=1.0, gamma=0.0):
        self.name = name

        # INPUT DIMENSIONS
        self.n_risks = n_risks
        self.time_horizon = time_horizon

        # NETWORK HYPER-PARAMETERS
        self.mb_size = mb_size
        self.h_dim_shared = h_dim_shared
        self.h_dim_cs = h_dim_cs
        self.num_layers_shared = num_layers_shared
        self.num_layers_cs = num_layers_cs
        self.keep_prob = keep_prob
        self.lr_train = lr_train

        # REGULARIZATION PARAMETERS
        self.active_fn = active_fn
        self.initial_W = tf.initializers.GlorotUniform()
        self.reg_W = tf.keras.regularizers.L2(l2=1e-4)
        self.reg_W_out = tf.keras.regularizers.L1(l1=1e-4)
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma

        self.n_epochs = n_epochs
        self.delta = delta
        self.max_patience = max_patience

    def f_get_fc_mask1(self, time, label):
        """
            mask1 is required to get the log-likelihood loss
            mask1 size is [N, n_risks, time_horizon]
                if not censored : one element = 1 (0 elsewhere)
                if censored     : fill elements with 1 after the censoring time (for all events)
        """
        mask = np.zeros([np.shape(time)[0], self.n_risks, self.time_horizon])  # for the first loss function
        for i in range(np.shape(time)[0]):
            if label[i] != 0:  # not censored
                mask[i, int(label[i] - 1), int(time[i])] = 1
            else:  # label[i,2]==0: censored
                mask[i, :, int(time[i] + 1):] = 1  # fill 1 until from the censoring time (to get 1 - \sum F)
        return mask

    def f_get_fc_mask2(self, time, meas_time):
        """
            mask2 is required calculate the ranking loss (for pair-wise comparision)
            mask2 size is [N, num_Category].
            - For longitudinal measurements:
                 1's from the last measurement to the event time (exclusive and inclusive, respectively)
                 denom is not needed since comparing is done over the same denom
            - For single measurement:
                 1's from start to the event time(inclusive)
        """
        mask = np.zeros([np.shape(time)[0], self.time_horizon])  # for the first loss function
        if np.shape(meas_time):  # longitudinal measurements
            for i in range(np.shape(time)[0]):
                t1 = int(meas_time[i, 0])  # last measurement time
                t2 = int(time[i])  # censoring/event time
                mask[i, (t1 + 1):(t2 + 1)] = 1  # this excludes the last measurement time and includes the event time
        else:  # single measurement
            for i in range(np.shape(time)[0]):
                t = int(time[i])  # censoring/event time
                mask[i, :(t + 1)] = 1  # this excludes the last measurement time and includes the event time
        return mask

    def _f_get_minibatch(self, k, x, label, time):
        label = np.asarray(label)
        idx = range(k*self.mb_size, (k+1)*self.mb_size)
        if max(list(idx)) >= len(x):
            excess = max(list(idx)) - len(x) + 1
            data_in_range = self.mb_size - excess
            base_idx = range(len(x)-data_in_range, len(x))
            extra_idx = np.random.choice(x.shape[0], excess, replace=True)
            batch_idx = np.append(list(base_idx), extra_idx)
        else:
            batch_idx = idx
        x_mb = x[batch_idx, :].astype(np.float32)
        k_mb = label.reshape(np.shape(label)[0], 1)[batch_idx, :].astype(np.float32)  # censoring(0)/event(1,2,..) label
        t_mb = time.reshape(np.shape(time)[0], 1)[batch_idx, :].astype(np.float32)
        batch_mask1 = self.f_get_fc_mask1(time[batch_idx], label[batch_idx])
        batch_mask2 = self.f_get_fc_mask2(time[batch_idx], -1)
        m1_mb = batch_mask1.astype(np.float32)  
        m2_mb = batch_mask2.astype(np.float32)
        return tf.convert_to_tensor(x_mb), tf.convert_to_tensor(k_mb), tf.convert_to_tensor(t_mb), tf.convert_to_tensor(m1_mb), tf.convert_to_tensor(m2_mb)

    def _make_model(self):
        input_shared = Input(shape=(self.n_features,), name='SharedNet_input')
        self.shared_net = create_FCNet(input_shared, self.num_layers_shared, self.h_dim_shared, self.active_fn,
                                  self.h_dim_shared, self.active_fn, w_init=self.initial_W, keep_prob=self.keep_prob, 
                                  w_reg=self.reg_W, name='SharedNet')

        # (num_layers_cs) layers for cause-specific (n_risks subNets)
        cs_outputs = []
        for r in range(self.n_risks):
            cs_net = create_FCNet(input_shared, self.num_layers_cs, self.h_dim_cs, self.active_fn, self.h_dim_cs,
                                  self.active_fn, w_init=self.initial_W, keep_prob=self.keep_prob, w_reg=self.reg_W,
                                  extra_input=self.shared_net.output,
                                  name='CauseSpecificNet_Event' + str(r+1))
            cs_outputs.append(cs_net.output)

        if self.n_risks == 1:
            out = Dropout(1-self.keep_prob)(cs_outputs[0])
        else:
            concatenated = Concatenate()(cs_outputs)
            out = Dropout(1-self.keep_prob)(concatenated)
        out = Dense(self.n_risks * self.time_horizon, activation='softmax',
                    kernel_initializer=self.initial_W, kernel_regularizer=self.reg_W_out)(out)
        out = Reshape((self.n_risks, self.time_horizon))(out)
        self.out_model = Model(inputs=self.shared_net.input, outputs=out, name='DeepHit')


        self.solver = tf.optimizers.Adam(learning_rate=self.lr_train)
        self.out_model.compile(loss=self.total_loss, optimizer=self.solver)

    # LOSS-FUNCTION 1 -- Log-likelihood loss
    def loss_log_likelihood(self, pred, events, fc_mask1):
        I_1 = tf.sign(events)

        # for uncensored: log P(T=t,K=k|x)
        tmp1 = tf.reduce_sum(tf.reduce_sum(fc_mask1 * pred, axis=2), axis=1,
                             keepdims=True)
        tmp1 = I_1 * log(tmp1)

        # for censored: log \sum P(T>t|x)
        tmp2 = tf.reduce_sum(tf.reduce_sum(fc_mask1 * pred, axis=2), axis=1,
                             keepdims=True)
        tmp2 = (1. - I_1) * log(tmp2)

        return - tf.reduce_mean(tmp1 + 1.0 * tmp2)
    
    # LOSS-FUNCTION 2 -- Ranking loss
    def loss_ranking(self, pred, times, events, fc_mask2):
        sigma1 = tf.constant(0.1, dtype=tf.float32)

        eta = []
        for e in range(self.n_risks):
            one_vector = tf.ones_like(times, dtype=tf.float32)
            I_2 = tf.cast(tf.equal(events, e + 1), dtype=tf.float32)  # indicator for event
            I_2 = tf.linalg.diag(tf.squeeze(I_2))
            tmp_e = tf.reshape(tf.slice(pred, [0, e, 0], [-1, 1, -1]),
                               [-1, self.time_horizon])  # event specific joint prob.

            R = tf.matmul(tmp_e, tf.transpose(fc_mask2))  # no need to divide by each individual dominator
            # r_{ij} = risk of i-th pat based on j-th time-condition (last meas. time ~ event time) , i.e. r_i(T_{j})

            diag_R = tf.reshape(tf.linalg.diag_part(R), [-1, 1])
            R = tf.matmul(one_vector, tf.transpose(diag_R)) - R  # R_{ij} = r_{j}(T_{j}) - r_{i}(T_{j})
            R = tf.transpose(R)  # Now, R_{ij} (i-th row j-th column) = r_{i}(T_{i}) - r_{j}(T_{i})

            T = tf.nn.relu(
                tf.sign(tf.matmul(one_vector, tf.transpose(times)) - tf.matmul(times, tf.transpose(one_vector))))
            # T_{ij}=1 if t_i < t_j  and T_{ij}=0 if t_i >= t_j

            T = tf.matmul(I_2, T)  # only remains T_{ij}=1 when event occurred for subject i

            tmp_eta = tf.reduce_mean(T * tf.exp(-R / sigma1), axis=1, keepdims=True)

            eta.append(tmp_eta)
        eta = tf.stack(eta, axis=1)  # stack referenced on subjects
        eta = tf.reduce_mean(tf.reshape(eta, [-1, self.n_risks]), axis=1, keepdims=True)

        return tf.reduce_sum(eta)  # sum over n_riskss

    # LOSS-FUNCTION 3 -- Calibration Loss
    def loss_calibration(self, pred, times, events, fc_mask2):
        eta = []
        for e in range(self.n_risks):
            one_vector = tf.ones_like(times, dtype=tf.float32)
            I_2 = tf.cast(tf.equal(events, e + 1), dtype=tf.float32)  # indicator for event
            tmp_e = tf.reshape(tf.slice(pred, [0, e, 0], [-1, 1, -1]),
                               [-1, self.time_horizon])  # event specific joint prob.

            r = tf.reduce_sum(tmp_e * fc_mask2, axis=0)  # no need to divide by each individual dominator
            tmp_eta = tf.reduce_mean((r - I_2) ** 2, axis=1, keepdims=True)

            eta.append(tmp_eta)
        eta = tf.stack(eta, axis=1)  # stack referenced on subjects
        eta = tf.reduce_mean(tf.reshape(eta, [-1, self.n_risks]), axis=1, keepdims=True)

        return tf.reduce_sum(eta)  # sum over n_risks
    
    def total_loss(self, pred, times, events, fc_mask1, fc_mask2):
        base_loss = self.alpha * self.loss_log_likelihood(pred, events, fc_mask1) \
                    + self.beta * self.loss_ranking(pred, times, events, fc_mask2) + tf.math.add_n(self.out_model.losses)
        if self.gamma > 0:
            return base_loss + self.gamma * self.loss_calibration(pred, times, events, fc_mask2) 
        else:
            return base_loss

    def train_step(self, x, times, events, fc_mask1, fc_mask2):
        with tf.GradientTape() as tape:
            pred = tf.convert_to_tensor(self.out_model(x, training=True))
            loss = self.total_loss(pred, times, events, fc_mask1, fc_mask2)
        gradients = tape.gradient(loss, self.out_model.trainable_variables)
        self.solver.apply_gradients((grad, var) 
                                    for (grad, var) in zip(gradients, self.out_model.trainable_variables) 
                                    if grad is not None)
        return loss

    def _set_training(self, X):
        if self.mb_size is None:
            self.mb_size = int(max(16, min(512, np.power(2,np.floor(np.log2(X.shape[0]*0.1))))))
        if self.n_epochs is None:
            self.n_epochs = int(max(self.max_patience, np.ceil(1000/(X.shape[0]/self.mb_size))))
        
    
    def update_loss(self, X):
            
        epoch_loss_avg = tf.keras.metrics.Mean()
    
        np.random.shuffle(X)
        train_data = X[:, :-2]
        train_time = X[:, -2]
        train_label = X[:, -1].astype(int)
        for k in range(int(np.ceil(np.shape(train_data)[0]/self.mb_size))):
            x_mb, k_mb, t_mb, m1_mb, m2_mb = self._f_get_minibatch(k, train_data, train_label, train_time)

            loss_value = self.train_step(x_mb, t_mb, k_mb, m1_mb, m2_mb)
            epoch_loss_avg.update_state(loss_value)  # Add current batch loss

        self.loss.append(epoch_loss_avg.result())

    def callback(self, ref_train_ibs, loc_patience):
        if np.abs(self.training_error[-1] - ref_train_ibs) >= self.delta:
            ref_train_ibs = self.training_error[-1]
            loc_patience = 0
        else:
            loc_patience += 1
        return ref_train_ibs, loc_patience

    def fit_with_validation(self, X, X_val):
        
        tf.keras.backend.clear_session()

        X_train_batch = X.copy() 
        self.n_features=np.shape(X[:, :-2])[1]
        self._make_model()
        self._set_training(X)
        
        ref_train_ibs = 1
        loc_patience = 0
        self.loss = []
        self.training_error = []
        self.validation_error = []

        for i in range(self.n_epochs):
            self.update_loss(X_train_batch)
            
            # PREDICTION
            pred_train = self.predict(X_train_batch)
            pred_val = self.predict(X_val)

            # EVALUATION ON TRAIN                               
            self.training_error.append(weighted_brier_score(X_train_batch[:,-2:], pred_train, competing_risk=1, eval_times=[365]))
            
            # EVALUATION ON VALIDATION                                  
            self.validation_error.append(weighted_brier_score(X_val[:,-2:], pred_val, competing_risk=1, eval_times=[365]))

            # CHECK EARLY STOPPING
            ref_train_ibs, loc_patience = self.callback(ref_train_ibs, loc_patience)
            if loc_patience > self.max_patience:
                break

    def fit(self, X, y=None, X_val=None):
        
        tf.keras.backend.clear_session()

        X_train_batch = X.copy()
        if X_val is None:
            X_val = X
        self.n_features=np.shape(X[:, :-2])[1]
        self._make_model()
        self._set_training(X)
        
        ref_train_ibs = 1
        loc_patience = 0
        self.loss = []
        self.training_error = []

        for i in range(self.n_epochs):
            self.update_loss(X_train_batch)
            pred_val = self.predict(X_val)
            self.training_error.append(weighted_brier_score(X_val[:,-2:], pred_val, competing_risk=1, eval_times=[365]))

            ref_train_ibs, loc_patience = self.callback(ref_train_ibs, loc_patience)
            if loc_patience > self.max_patience:
                break

    def load_from_save(self, model_url): 
        self.out_model = tf.keras.models.load_model(model_url, compile=False)

    def predict(self, X):
        m = 1000
        q = int(X.shape[0]/m)
        r = np.mod(X.shape[0], m)
        preds = np.concatenate([self.out_model.predict(X[i*m:(i+1)*m,:-2]) for i in range(max(q,1))], axis=0)
        if q > 0 and r > 0:
            preds = np.concatenate([preds, self.out_model.predict(X[q*m:q*m + r,:-2])], axis=0)
        return preds

    def predict_survival_time(self, X, eval_time, competing_risk):
        y_pred = self.predict(X)

        def cumulative_incidence_function(pred_prob, i, k, t):
            z = int(t) + 1
            return np.sum(pred_prob[i, k - 1, :z])
        S = lambda t, pred, n, e: 1 - cumulative_incidence_function(y_pred, n, e, t)

        survival_times = np.zeros([y_pred.shape[0]])
        for i in range(y_pred.shape[0]):
            survival_times[i] = integrate.quad(S, 0, eval_time, args=(y_pred, i, competing_risk), full_output=1)[0]

        return survival_times


class NullModel(BaseEstimator, RegressorMixin):
    def __init__(self, n_risks=1, time_horizon=100):
        self.time_horizon = time_horizon
        self.n_risks = n_risks

    def fit(self, X, y=None):
        return self

    def predict(self, X, y=None):
        pred = np.zeros([X.shape[0], self.n_risks, self.time_horizon])
        for i in range(pred.shape[0]):
            for k in range(pred.shape[1]):
                a = np.random.randint(0, self.time_horizon, self.time_horizon)
                p = a / np.sum(a)
                pred[i, k, :] = p
        return pred


