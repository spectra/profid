'''
This provide time-dependent Concordance index and Brier Score:
    - Use weighted_c_index and weighted_brier_score, which are the unbiased estimates.
    
See equations and descriptions eq. (11) and (12) of the following paper:
    - C. Lee, W. R. Zame, A. Alaa, M. van der Schaar, "Temporal Quilting for Survival Analysis", AISTATS 2019
'''

import numpy as np
from lifelines import KaplanMeierFitter
from sksurv.util import Surv
from sksurv.metrics import integrated_brier_score as integrated_brier_score_ipcw


### C(t)-INDEX CALCULATION
def c_index(Prediction, Time_survival, Death, Time):
    '''
        This is a cause-specific c(t)-index
        - Prediction      : risk at Time (higher --> more risky)
        - Time_survival   : survival/censoring time
        - Death           :
            > 1: death
            > 0: censored (including death from other cause)
        - Time            : time of evaluation (time-horizon when evaluating C-index)
    '''
    N = len(Prediction)
    A = np.zeros((N, N))
    Q = np.zeros((N, N))
    N_t = np.zeros((N, N))
    Num = 0
    Den = 0
    for i in range(N):
        A[i, np.where(Time_survival[i] < Time_survival)] = 1
        Q[i, np.where(Prediction[i] > Prediction)] = 1

        if (Time_survival[i] <= Time and Death[i] == 1):
            N_t[i, :] = 1

    Num = np.sum(((A) * N_t) * Q)
    Den = np.sum((A) * N_t)

    if Num == 0 and Den == 0:
        result = -1  # not able to compute c-index!
    else:
        result = float(Num / Den)

    return result


### BRIER-SCORE
def brier_score(Prediction, Time_survival, Death, Time):
    N = len(Prediction)
    y_true = ((Time_survival <= Time) * Death).astype(float)

    return np.mean((Prediction - y_true) ** 2)

    # result2[k, t] = brier_score_loss(risk[:, k], ((te_time[:,0] <= eval_horizon) * (te_label[:,0] == k+1)).astype(int))


##### WEIGHTED C-INDEX & BRIER-SCORE
def CensoringProb(Y, T):
    T = T.reshape([-1])  # (N,) - np array
    Y = Y.reshape([-1])  # (N,) - np array

    kmf = KaplanMeierFitter()
    kmf.fit(T, event_observed=(Y == 0).astype(int))  # censoring prob = survival probability of event "censoring"
    G = np.asarray(kmf.survival_function_.reset_index()).transpose()
    G[1, G[1, :] == 0] = G[1, G[1, :] != 0][-1]  # fill 0 with ZoH (to prevent nan values)

    return G


### C(t)-INDEX CALCULATION: this account for the weighted average for unbaised estimation
def weighted_c_index(Prediction, T_test, Y_test, T_train=None, Y_train=None, Time=365):
    '''
        This is a cause-specific c(t)-index
        - Prediction      : risk at Time (higher --> more risky)
        - Time_survival   : survival/censoring time
        - Death           :
            > 1: death
            > 0: censored (including death from other cause)
        - Time            : time of evaluation (time-horizon when evaluating C-index)
    '''
    if T_train is not None and Y_train is not None:
        G = CensoringProb(Y_train, T_train)
    else:
        G = CensoringProb(Y_test, T_test)

    N = len(Prediction)
    A = np.zeros((N, N))
    Q = np.zeros((N, N))
    N_t = np.zeros((N, N))
    for i in range(N):
        tmp_idx = np.where(G[0, :] >= T_test[i])[0]

        if len(tmp_idx) == 0:
            W = (1. / G[1, -1]) ** 2
        else:
            W = (1. / G[1, tmp_idx[0]]) ** 2

        A[i, np.where(T_test[i] < T_test)] = 1. * W
        Q[i, np.where(Prediction[i] > Prediction)] = 1.  # give weights

        if (T_test[i] <= Time and Y_test[i] == 1):
            N_t[i, :] = 1.

    Num = np.sum(((A) * N_t) * Q)
    Den = np.sum((A) * N_t)

    if Num == 0 and Den == 0:
        result = -1  # not able to compute c-index!
    else:
        result = float(Num / Den)

    return result


# this account for the weighted average for unbaised estimation
def base_weighted_brier_score(Prediction, T_test, Y_test, T_train=None, Y_train=None, Time=365):
    if T_train is not None and Y_train is not None:
        G = CensoringProb(Y_train, T_train)
    else:
        G = CensoringProb(Y_test, T_test)
    N = len(Prediction)

    W = np.zeros(len(Y_test))
    Y_tilde = (T_test > Time).astype(float)

    for i in range(N):
        tmp_idx1 = np.where(G[0, :] >= T_test[i])[0]
        tmp_idx2 = np.where(G[0, :] >= Time)[0]

        if len(tmp_idx1) == 0:
            G1 = G[1, -1]
        else:
            G1 = G[1, tmp_idx1[0]]

        if len(tmp_idx2) == 0:
            G2 = G[1, -1]
        else:
            G2 = G[1, tmp_idx2[0]]
        W[i] = (1. - Y_tilde[i]) * float(Y_test[i]) / G1 + Y_tilde[i] / G2

    # y_true = ((T_test <= Time) * Y_test).astype(float)

    return np.mean(W * (Y_tilde - (1. - Prediction)) ** 2)

def get_weighted_brier_score(y, y_pred, k=None, eval_times=None):
    va_time = y[:, 0].astype(int)
    va_label = y[:, 1].astype(int)
    if eval_times is None:
        y_not_censored = y[(y[:, 1] == k)]
        eval_time_k = y_not_censored[:, 0].max()
    else:
        eval_time_k = eval_times[k-1]
    z = int(eval_time_k) + 1
    risk = np.sum(y_pred[:, k-1, :z], axis=1)  # risk score until eval_time for all competing risks
    return base_weighted_brier_score(risk, va_time, (va_label == k).astype(int), Time=eval_time_k)


def weighted_brier_score(y, y_pred, competing_risk=None, eval_times=None):
    n_risks = y_pred.shape[1]
    if competing_risk is None:
        va_result = np.zeros([n_risks])
        for k in range(1, n_risks+1):
            va_result[k-1] = get_weighted_brier_score(y, y_pred, k, eval_times)
        score = np.mean(va_result)
    else:
        score = get_weighted_brier_score(y, y_pred, competing_risk, eval_times)
    return score


def get_weighted_c_index(y, y_pred, k, eval_times=None):
    va_time = y[:, 0].astype(int)
    va_label = y[:, 1].astype(int)
    if eval_times is None:
        y_not_censored = y[(y[:, 1] == k)]
        eval_time_k = y_not_censored[:, 0].max()
    else:
        eval_time_k = eval_times[k-1]
    z = int(eval_time_k) + 1
    risk = np.sum(y_pred[:, k-1, :z], axis=1)  # risk score until eval_time for all competing risks
    return weighted_c_index(risk, va_time, (va_label == k).astype(int), Time=eval_time_k)


def weighted_concordance_index_score(y, y_pred, competing_risk=None, eval_times=None):
    n_risks = y_pred.shape[1]
    if competing_risk is None:
        va_result = np.zeros([n_risks])
        for k in range(1, n_risks+1):
            va_result[k-1] = get_weighted_c_index(y, y_pred, k, eval_times)
        score = np.mean(va_result)
    else:
        score = get_weighted_c_index(y, y_pred, competing_risk, eval_times)
    return score


def get_ibs(y, y_pred, k, eval_times=None):
    y_cens = y[(y[:, 1] != k)]
    t_cens_max = y_cens[:, 0].max()
    cens_indexes = np.where(y[:, 0] <= t_cens_max)
    y_trunc = y[cens_indexes]
    y_surv = y_trunc[np.where(y_trunc[:, 1] == k)]
    t_min = y_surv[:, 0].min()
    if eval_times is None:
        t_max = y_surv[:, 0].max()
    else:
        t_max = eval_times[k-1]
    survival_times = np.arange(int(t_min), int(t_max))
    y_pred_k_cs = np.cumsum(y_pred[:, k-1, :], axis=1)[cens_indexes]
    preds = y_pred_k_cs[:, int(t_min):int(t_max)]
    y_test = Surv.from_arrays((y_trunc[:, 1] == k).astype(int), y_trunc[:, 0].astype(int))
    return integrated_brier_score_ipcw(y_test, y_test, 1-preds, survival_times)


def integrated_brier_score(y, y_pred, competing_risk=None, eval_times=None):
    if competing_risk is None:
        n_risks = y_pred.shape[1]
        brier_scores = np.zeros([n_risks])
        for k in range(1, n_risks+1):
            brier_scores[k-1] = get_ibs(y, y_pred, k, eval_times)
        score = np.mean(brier_scores)
    else:
        score = get_ibs(y, y_pred, competing_risk, eval_times)
    return score