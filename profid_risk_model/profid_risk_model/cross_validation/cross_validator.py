from sklearn.model_selection import KFold, RandomizedSearchCV, GridSearchCV, cross_validate, StratifiedKFold, train_test_split
from sklearn.metrics import make_scorer
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler
import numpy as np
import pandas as pd
from hyperopt import fmin, tpe, Trials, space_eval
from functools import partial
from profid_risk_model.deephit import DeepHit
from profid_risk_model.deephit.utils_eval import weighted_concordance_index_score, integrated_brier_score, weighted_brier_score
import logging

class CrossValidator(object):
    def __init__(self, space={}, model_pipe=None, search_mode='grid', outer_splits=5, inner_splits=5, random_state=None,
                 max_iter=None, n_permutations=5, shuffle=False, n_jobs=1):
        self.space = space
        self.model_pipe = model_pipe
        self.search_mode = search_mode
        self.outer_splits = outer_splits
        self.inner_splits = inner_splits
        self.random_state = random_state
        self.max_iter = max_iter
        self.n_permutations = n_permutations
        self.shuffle = shuffle
        self.n_jobs = n_jobs
        self.search = None
        self.nested_scores_ = None

    @staticmethod
    def fit_pipeline(X_train, params, X_val=None):
        T = int(X_train['Survival_time'].max()*1.1)

        scaler = ColumnTransformer([('std', StandardScaler(), X_train.columns[:-2])], remainder='passthrough')
        pipe = Pipeline(steps=[('scaler', scaler), ('dnn', DeepHit(**params, n_risks=X_train['Status'].nunique()-1,
                                                                   time_horizon=T, lr_train=0.001, keep_prob=0.6))])
        return pipe.fit(X_train, dnn__X_val = X_val.values)

    @staticmethod
    def get_best_features(X_val, pipe, m, n_permutations):
        y_pred =  pipe.predict(X_val)
        ibs_ref = weighted_brier_score(X_val.iloc[:,-2:].values, y_pred, competing_risk=1, eval_times=[365])
        scores = {c:0 for c in  X_val.columns[:-2]}
        if X_val.shape[0] > 5000 :
            X_val_train, X_val_test  = train_test_split(X_val, test_size=5000, stratify=X_val.Status)
        else:
            X_val_test = X_val.copy()
        X = X_val_test.copy()
        for c in X_val.columns[:-2]:
            scores_col = []
            for i in range(n_permutations):
                X[c] = np.random.permutation(X_val_test[c].values)
                pred = pipe.predict(X)
                scores_col.append(weighted_brier_score(X.iloc[:,-2:].values, pred, competing_risk=1, eval_times=[365])-ibs_ref)
            scores[c] = np.mean(scores_col) 
        scores_sorted = sorted(scores.items(), key=lambda x: x[1], reverse=True)
        return [s[0] for s in scores_sorted[:m]], scores_sorted[:m]


    def score_best_features(self, X, k, hyperparams):
        params = {'num_layers_shared': hyperparams['num_layers_shared'], 
                  'num_layers_cs': hyperparams['num_layers_cs'],
                  'h_dim_shared': hyperparams['h_dim_shared'],
                  'h_dim_cs': hyperparams['h_dim_cs']}

        scores = []
        logging.warning('Testing params:')
        logging.warning(hyperparams)
        cv_index = 0
        for train_index, val_index in self.inner_cv.split(X, X.Status):
            cv_index += 1
            logging.warning('Fitting fold {} out of {}'.format(cv_index, self.inner_splits))
            X_train = X.iloc[train_index]
            X_val = X.iloc[val_index]
 
            pipe_ref = CrossValidator.fit_pipeline(X_train, params, X_val=X_val)

            m = hyperparams.pop('m', None)
            if m is not None and m < X_val.shape[1] - 2:
                f, f_scores = CrossValidator.get_best_features(X_val, pipe_ref, m, self.n_permutations)
                X_train = pd.concat([X_train[f], X_train[['Survival_time', 'Status']]], axis=1)
                X_val = pd.concat([X_val[f], X_val[['Survival_time', 'Status']]], axis=1)
                pipe_best = CrossValidator.fit_pipeline(X_train, params, X_val=X_val)
            else:
                pipe_best = pipe_ref

            pred_val =  pipe_best.predict(X_val)

            ibs = integrated_brier_score(X_val.iloc[:,-2:].values, pred_val, competing_risk=k)
            scores.append(ibs)
            
        score = np.mean(scores)
        logging.warning('Trial score: {}'.format(score))

        return score


    def get_hyperopt_best_params(self, X, k):
        np.random.seed(self.random_state)

        trials = Trials()
        objective = partial(self.score_best_features, X, k)

        best=fmin(fn=objective, 
                space=self.space, 
                algo=tpe.suggest, 
                max_evals=self.max_iter, 
                trials=trials, 
                rstate=np.random.RandomState(self.random_state) 
            )
        
        return best


    def fit_best(self, X, k):
        if X.shape[0]*0.2 > 10000:
            X_train, X_val  = train_test_split(X, test_size=10000, stratify=X.Status)
        else:
            X_train, X_val  = train_test_split(X, test_size=0.2, stratify=X.Status)

        best_params = space_eval(self.space, self.hyperopt_best_params)
        m = best_params.pop('m', X.shape[1] - 2)
        pipe_ref = CrossValidator.fit_pipeline(X, best_params, X_val=X_val)
        f, f_scores = CrossValidator.get_best_features(X_val, pipe_ref, m, self.n_permutations)
        self.best_features = f
        self.best_scores = [s[1] for s in f_scores]
        
        X_best = pd.concat([X[self.best_features], X[['Survival_time', 'Status']]], axis=1)
        X_val_best = pd.concat([X_val[self.best_features], X_val[['Survival_time', 'Status']]], axis=1)
        
        self.best_model = CrossValidator.fit_pipeline(X_best, best_params, X_val=X_val_best)
        y_pred = self.best_model.predict(X_best)
        self.val_error = integrated_brier_score(X_best.iloc[:,-2:].values, y_pred, competing_risk=k)

        return self

    @staticmethod
    def choose_scorer(score, k=None, eval_times=None):
        if score == "c_index_weighted":
            my_scorer_index = make_scorer(weighted_concordance_index_score, competing_risk=k, eval_times=eval_times, greater_is_better=True)
        elif score =="integrated_brier_score":
            my_scorer_index = make_scorer(integrated_brier_score, competing_risk=k, eval_times=eval_times, greater_is_better=False)
        else:
            raise TypeError('You need to specify score ("c_index_weighted" or "integrated_brier_score")')
        return my_scorer_index


    def fit_search(self, X, score, k, eval_times):
        my_scorer_index = CrossValidator.choose_scorer(score, k, eval_times)
        if self.search_mode == "grid":
            self.search = GridSearchCV(self.model_pipe, self.space, cv=self.inner_cv.split(X, X.Status), n_jobs=self.n_jobs, scoring=my_scorer_index, verbose=1)    
        elif self.search_mode == "random":
            self.search = RandomizedSearchCV(self.model_pipe, self.space, cv=self.inner_cv.split(X, X.Status), n_jobs=self.n_jobs, scoring=my_scorer_index, verbose=1, n_iter=self.max_iter)
        self.search.fit(X, X.iloc[:, -2:].values)


    def validate(self, X, score=None, k=None, eval_times=None):
        self.inner_cv = StratifiedKFold(n_splits=self.inner_splits, shuffle=self.shuffle, random_state=self.random_state)
        if self.search_mode in ['grid', 'random']:
            self.fit_search(X, score, k, eval_times)
        elif self.search_mode == "hyperopt":
            self.hyperopt_best_params = self.get_hyperopt_best_params(X, k)
            self.fit_best(X, k=1)
        else:
            raise TypeError('You need to specify search_mode ("grid", "random" or "hyperopt")')
        return self


    def validate_nested(self, X, score, k=None, eval_times=None):
        my_scorer_index = CrossValidator.choose_scorer(score, k, eval_times)
        inner_cv = KFold(n_splits=self.inner_splits, shuffle=self.shuffle, random_state=self.random_state)
        if self.search_mode == "grid":
            search = GridSearchCV(self.model_pipe, self.space, cv=inner_cv, n_jobs=1, scoring=my_scorer_index,
                                  verbose=1)
        if self.search_mode == "random":
            search = RandomizedSearchCV(self.model_pipe, self.space, cv=inner_cv, n_jobs=1, scoring=my_scorer_index,
                                        verbose=1)
        outer_cv = KFold(n_splits=self.outer_splits, shuffle=self.shuffle, random_state=self.random_state)
        self.nested_scores_ = cross_validate(search, X, X[:, -2:], cv=outer_cv)
        return self


